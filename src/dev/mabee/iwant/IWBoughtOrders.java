package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ed on 02.12.2014.
 */
public class IWBoughtOrders extends FragmentActivity {
    IWAdapter iwAdapter;
    ListView listView;
    TextView textFind;

    BroadcastReceiver broadcastReceiver;

    ArrayList<IWAdapter.Item> items = new ArrayList<IWAdapter.Item>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boughtorders_screen);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.relog"));

        listView = (ListView) findViewById(R.id.boughtorders_screen_listview);
        textFind = (TextView) findViewById(R.id.boughtorders_screen_textfind);
        iwAdapter = new IWAdapter(this);
        listView.setAdapter(iwAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(IWBoughtOrders.this, IWInfoOrder.class);
                intent.putExtra("text",items.get(position).text);
                intent.putExtra("phone",items.get(position).phone);
                intent.putExtra("fio",items.get(position).fio);
                intent.putExtra("call_time_start",items.get(position).call_time_start);
                intent.putExtra("call_time_end",items.get(position).call_time_end);
                intent.putExtra("region",items.get(position).region);
                intent.putExtra("city",items.get(position).city);
                intent.putExtra("entity",items.get(position).entity);

                startActivity(intent);
            }
        });

        final ProgressDialog progressDialog = new ProgressDialog(IWBoughtOrders.this);
        progressDialog.setMessage("Синхронизация");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        SharedPreferences preferences = getSharedPreferences("iwant_data",MODE_PRIVATE);
        String session = preferences.getString(IWUserModel.sessionID,"none");

        IWServer.getInstance().sendRequestPayedWants(session, new IWCallback() {
            @Override
            public void call(Response data) {
                if (data.code()==508) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            AlertDialog.Builder builder = new AlertDialog.Builder(IWBoughtOrders.this);
                            builder.setTitle("Ошибка");
                            builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent("dev.mabee.relog");
                                    sendBroadcast(intent);
                                }
                            });
                            builder.create().show();
                        }
                    });
                    return;
                }
                try {
                    String resp = data.body().string();
                    if (resp.length() > 0) {
                        items.clear();
                        JSONObject objectWants = new JSONObject(resp);
                        JSONArray array = objectWants.getJSONArray("wishes");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject objectWant = array.getJSONObject(i);

                            String text        = objectWant.getString("text");
                            String phone       = objectWant.getString("phone_number");
                            String first_name  = objectWant.getString("first_name");
                            String last_name   = objectWant.getString("last_name");
                            String second_name = objectWant.getString("second_name");
                            String city        = objectWant.getString("city");
                            String region      = objectWant.getString("region");
                            String tags        = objectWant.getString("tags");
                            String call_time_start = objectWant.getString("call_time_start");
                            String call_time_end    = objectWant.getString("call_time_end");
                            String entity_name = "";
                            if (!objectWant.isNull("entity_name")) {
                               entity_name = objectWant.getString("entity_name");
                            }
                            items.add(new IWAdapter.Item(last_name+ " "  +first_name + " " +second_name,phone,text,call_time_start,call_time_end,region,city, entity_name,tags));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                IWBoughtOrders.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (items.size()>0) {
                            textFind.setVisibility(View.GONE);
                        } else {
                            textFind.setVisibility(View.VISIBLE);
                        }
                        progressDialog.dismiss();
                        iwAdapter.swapData(items);
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {}
    }

    public void iwBoughtOrders_ClickBack(View view) {
        onBackPressed();
    }
    public static class IWAdapter extends BaseAdapter {
        ArrayList<Item> items = new ArrayList<Item>();

        public static class  Item implements Serializable {

            public String text;
            public String city;
            public String region;
            public String call_time_start;
            public String call_time_end;
            public String fio;
            public String phone;
            public String entity;
            public String tags;
            public Item(String fio,String phone,String text,String call_time_start, String call_time_end, String region, String city, String entity, String tags) {
                this.text = text;
                this.city = city;
                this.region = region;
                this.call_time_end = call_time_end;
                this.call_time_start = call_time_start;
                this.fio = fio;
                this.phone = phone;
                this.entity = entity;
                this.tags = tags;
            }
        }

        private Context context;

        public IWAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        public void swapData(ArrayList<Item> list) {
            this.items = list;
            notifyDataSetChanged();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.boughtorders_screen_row, parent, false);

            TextView textText   = (TextView) rowView.findViewById(R.id.boughtorders_screen_row_text);
            TextView textCity   = (TextView) rowView.findViewById(R.id.boughtorders_screen_row_city);
            TextView textRegion = (TextView) rowView.findViewById(R.id.boughtorders_screen_row_region);

            Item item = items.get(position);
            textCity.setText(item.city);
            if (!item.tags.equals("")) {
                textText.setText(item.tags);
            } else {
                textText.setText(item.text);
            }

            textRegion.setText(item.region);

            return rowView;
        }
    }

}
