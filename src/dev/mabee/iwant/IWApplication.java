package dev.mabee.iwant;

import android.app.Application;
import android.content.*;
import android.os.Bundle;
import android.util.Log;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONObject;
import org.onepf.oms.OpenIabHelper;
import org.onepf.oms.appstore.googleUtils.*;
import org.onepf.oms.util.Logger;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ed on 12.12.2014.
 */
public class IWApplication extends Application {


    private String publicAPIKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAubWWLbcPosPGo+YV9lkHoJfScvbvSZ+KIDLLa8l5QyclPm4rVLk+SQSAu/u/jrQoyOtRT71RSrfLQ660zIMkUZDIM0xNX0XrjXWfu2kQxjaj0asRZbROyP23QihXb6FsEvsH2xmttAq9aGyrvg1fBjtpFn7u7meN9LmNzNWk0cgoXPmQjeRivl1XdM7FoILCE2m/Kgtw06H2zbsN8XoCoIuFnqtefnVGAutThihjo46N5b2rqFVa+TytO8dw0oilmhbGJ1gxEV5+HfVzYRmWiiN5pmNzk529fKwtgzQhiHTSGYw3IuJUj6C885WTw3j6zaBDfFnZnYQr09MSQMbr0QIDAQAB";
    private String yandexKey    = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk5PsYqT2i8vfFg3fVOePs4uNbLq2/LhVGF/Neg0F9+KHiWY5UPF8EVb8VTd19EhinKYOvWOdB0dGAMOpQgW+aABkEhKrofp0kL6bt11GLztTbMWCyioX/qAM3WrLR6byXgDlDLp7bOQ2aCmtYJR/eR5uAX006kOogME2a7rLlZTItasRXUbLiM1TrVQpZ3b79scKXV2ZBaz9OSbPvYgO8cur6bqijd71fAK0PbibdtDI9+bRNQiazPaYuKIECJSoONFnt7HdHxmskMjnAqUozC0HcedomUMyGq2Pge/9vyG4CTcdSX153hPO+CgSGCbpQf6ycncWP0RY+QBIVTOtjQIDAQAB";

    OpenIabHelper openIabHelper;
    HashMap<String, String> prices = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();

        IntentFilter filter = new IntentFilter("dev.mabee.relog");
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle exit  = intent.getExtras();
                if (exit==null) {
                    SharedPreferences.Editor editor = getSharedPreferences("iwant_data", MODE_PRIVATE).edit();
                    editor.clear();
                    editor.commit();

                    Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            }

        };
        registerReceiver(broadcastReceiver, filter);

        OpenIabHelper.Options.Builder IabBuilder = new OpenIabHelper.Options.Builder();
        Logger.setLoggable(true);

        // TODO: KEYS
        IabBuilder.addStoreKey(OpenIabHelper.NAME_GOOGLE, publicAPIKey);
        //IabBuilder.addStoreKey(OpenIabHelper.NAME_YANDEX, yandexKey);
        openIabHelper = new OpenIabHelper(getApplicationContext(), IabBuilder.build());
        setupIAB();
    }

    public void requestPrices() {


        SharedPreferences preferences = getSharedPreferences("iwant_data",MODE_PRIVATE);
        String session = preferences.getString(IWUserModel.sessionID, "none");

        // Subscribe
        IWServer.getInstance().sendRequestGetPriceSubscribe(session, new IWCallback() {
            @Override
            public void call(Response data) {
                try {
                    String pricees = data.body().string();
                    JSONObject jsonObject = new JSONObject(pricees);
                    final String SKU = jsonObject.getString("originalPriceId");

                    final ArrayList<String> listPrice = new ArrayList<>();
                    listPrice.add(SKU);

                     openIabHelper.queryInventoryAsync(true, listPrice, listPrice, new IabHelper.QueryInventoryFinishedListener() {
                        @Override
                        public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
                            if (iabResult.isSuccess()) {
                                String price;
                                SkuDetails skuDetails = inventory.getSkuDetails(SKU);
                                if (skuDetails != null) {
                                    price = skuDetails.getPrice();
                                    Log.i("mLogs","Price : " + price);
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
/*

        IWServer.getInstance().sendRequestGetPrice(session, wishStatus, new IWCallback() {

            @Override
            public void call(Response data) {
                try {
                    String dd = data.body().string();
                    JSONObject jsonObject = new JSONObject(dd);
                    Intent intent =  new Intent(IWAcceptWant.this,IWPayActivity.class);
                    intent.putExtra("uuid",uuid);
                    if (jsonObject.length()>1) {
                        intent.putExtra("free", true);
                    } else {
                        String SKU_FAKE  = jsonObject.getString("originalPriceId");
                        intent.putExtra("free", false);
                        intent.putExtra("SKU", SKU_FAKE);



                        Log.i("mLogs", "SKU : " + SKU_FAKE);
                    }
                    startActivityForResult(intent, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        */
    }

    public void setupIAB() {
        Log.i("mLogs", " Setup IAB begin : " + openIabHelper.getSetupState());
        if (!openIabHelper.setupSuccessful()) {
            openIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult iabResult) {
                    Log.i("mLogs", "Setup In App Billing : " + iabResult.getMessage());
                    if (iabResult.isSuccess()) {
                        consumeAll();
                    }
                }
            });
        }
    }

    public void consumeAll() {
        openIabHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {

                for (String sky : inventory.getAllOwnedSkus()) {
                    Purchase purchase = inventory.getPurchase(sky);
                    try {
                        openIabHelper.consume(purchase);
                    } catch (IabException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


}
