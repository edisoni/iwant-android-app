package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import dev.mabee.iwant.utils.IWWantModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Ed on 02.12.2014.
 */
public class IWViewedOrders extends FragmentActivity {

    IWAdapter adapter;
    ArrayList<IWAdapter.Item> items = new ArrayList<>();
    ListView listView;
    TextView textFind;
    BroadcastReceiver receiver;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewedorders_screen);
        listView = (ListView) findViewById(R.id.viewedorders_screen_listview);
        textFind = (TextView) findViewById(R.id.viewedorders_screen_textfind);
        adapter = new IWAdapter(this);
        listView.setAdapter(adapter);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("dev.mabee.relog"));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(IWViewedOrders.this, IWAcceptWant.class);
                intent.putExtra("desc", items.get(position).text);
                intent.putExtra("active", items.get(position).active);
                intent.putExtra("uuid", items.get(position).uuid);
                intent.putExtra("city", items.get(position).city);
                intent.putExtra("region", items.get(position).region);
                intent.putExtra("time", "С 19:00 до 18:00");
                intent.putExtra("wishStatus", "2");
                startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синхронизация");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        sync(progressDialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter("iw.orders");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("mLogs", "VIEWED");
                Bundle bundle = intent.getExtras();
                String uuid = bundle.getString("uuid");
                ArrayList<IWAdapter.Item> its = adapter.getItems();
                for (IWAdapter.Item item : its) {

                    if (item.uuid.equals(uuid)) {
                        item.active = "0";
                        break;
                    }
                }
                adapter.notifyDataSetChanged();
            }
        };
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(receiver);
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {
        }
    }

    public void sync(final ProgressDialog progressDialog) {
        SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);

        String session = preferences.getString(IWUserModel.sessionID, "none");


        IWServer.getInstance().sendRequestOrders(session, new IWCallback() {
            @Override
            public void call(Response data) {
                if (data.code() == 508) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            AlertDialog.Builder builder = new AlertDialog.Builder(IWViewedOrders.this);
                            builder.setTitle("Ошибка");
                            builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent("dev.mabee.relog");
                                    sendBroadcast(intent);
                                }
                            });
                            builder.create().show();

                        }
                    });
                    return;
                }
                try {
                    String resp = data.body().string();
                    if (resp.length() > 0) {
                        items.clear();
                        JSONObject objectWants = new JSONObject(resp);
                        JSONArray array = objectWants.getJSONArray("wishes");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject objectWant = array.getJSONObject(i);


                            String tags = objectWant.getString("tags");
                            String text = objectWant.getString(IWWantModel.text);
                            String uuid = objectWant.getString(IWWantModel.uuid);
                            String city = objectWant.getString("city");
                            String region = objectWant.getString("region");
                            String active = objectWant.getString("active");

                            items.add(new IWAdapter.Item(text, city, region, active, uuid, tags));
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int suze = adapter.swapData(items);
                        if (suze > 0) {
                            textFind.setVisibility(View.GONE);
                        } else {
                            textFind.setVisibility(View.VISIBLE);
                        }
                        progressDialog.dismiss();

                    }
                });
            }
        });
    }

    public void iwViewedOrders_ClickBack(View view) {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 5) {
            finish();
        }
    }

    static class MyComparator implements Comparator<IWAdapter.Item> {
        @Override
        public int compare(IWAdapter.Item item, IWAdapter.Item t1) {
            boolean b1 = item.active.equals("1");
            boolean b2 = t1.active.equals("1");

            long time1 = item.time;
            long time2 = t1.time;

            if (b1 && !b2) {
                return -1;
            } else if (!b1 && b2) {
                return +1;
            } else {
                if (time1>time2) {
                    return -1;
                } else {
                    return +1;
                }
            }
        }
    }

    public static class IWAdapter extends BaseAdapter {
        ArrayList<Item> items = new ArrayList<Item>();


        public static class Item implements Serializable {

            public String text;
            public String uuid;
            public String city;
            public String region;
            public String active;
            public String tags;
            public int time;

            public Item(String text, String city, String region, String active, String uuid, String tags) {
                this.text = text;
                this.city = city;
                this.region = region;
                this.active = active;
                this.uuid = uuid;
                this.tags = tags;
            }
        }

        private Context context;

        public ArrayList<Item> getItems() {
            return items;
        }

        public IWAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        public int swapData(ArrayList<Item> list) {
            this.items = list;
            SharedPreferences preferences = context.getSharedPreferences("story_orders", MODE_PRIVATE);
            ArrayList<String> lineOrders = new ArrayList<>();
            ArrayList<String> timesOrders = new ArrayList<>();
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(preferences.getString("orders", "none"));
                for (int i = 0; i < jsonArray.length(); i++) {
                    lineOrders.add(jsonArray.getJSONObject(i).getString("uuid"));
                    timesOrders.add(jsonArray.getJSONObject(i).getString("time"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Iterator<Item> iterator = this.items.iterator();
            while (iterator.hasNext()) {
                Item item = iterator.next();

                if (!lineOrders.contains(item.uuid)) {
                    iterator.remove();
                } else {
                    int index = lineOrders.indexOf(item.uuid);
                    int time = Integer.parseInt(timesOrders.get(index));
                    item.time = time;
                }
            }

            Collections.sort(items,new MyComparator());

            notifyDataSetChanged();
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.viewedorders_screen_row, parent, false);

            TextView textText = (TextView) rowView.findViewById(R.id.viewedorders_screen_row_text);
            TextView textCity = (TextView) rowView.findViewById(R.id.viewedorders_screen_row_city);
            TextView textRegion = (TextView) rowView.findViewById(R.id.viewedorders_screen_row_region);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.viewedorders_screen_row_image);

            Item item = items.get(position);
            textCity.setText(item.city);
            textRegion.setText(item.region);

            if (item.active.equals("0")) {
                imageView.setVisibility(View.VISIBLE);
                textText.setText(item.text);
                imageView.setImageResource(R.drawable.notactive);
                rowView.setBackgroundColor(context.getResources().getColor(R.color.grey));
            } else {
                imageView.setVisibility(View.VISIBLE);
                textText.setText(item.tags);
                imageView.setImageResource(R.drawable.activered);
                rowView.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            return rowView;
        }
    }

}
