package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import android.widget.*;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.internal.Platform;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Ed on 21.11.2014.
 */
public class IWExecutorsWantActivity extends FragmentActivity {

    ArrayList<IWAdapter.Item> items = new ArrayList<IWAdapter.Item>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.executorswant_screen);

        final IWAdapter adapter = new IWAdapter(this);
        final ListView listView = (ListView) findViewById(R.id.executorswant_screen_listview);
        final TextView textView = (TextView) findViewById(R.id.executorswant_screen_missexe);
        listView.setAdapter(adapter);
        final String hed = getIntent().getExtras().getString("HEADER");
        final boolean del = getIntent().getExtras().getBoolean("del", false);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!del) {
                    IWAdapter.Item it = (IWAdapter.Item) adapter.getItem(position);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    String str = "tel:" + it.getPhone();
                    intent.setData(Uri.parse(str));
                    startActivity(intent);
                } else {
                    IWAdapter.Item it = (IWAdapter.Item) adapter.getItem(position);
                    if (it.voiting.equals("1")) {
                        final ProgressDialog progressDialog = new ProgressDialog(IWExecutorsWantActivity.this);
                        progressDialog.setMessage("Синхронизация");
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.show();

                        final SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
                        String sid = preferences.getString(IWUserModel.sessionID, "none");
                        try {
                            JSONArray array = new JSONArray();
                            array.put(items.get(position).uuid);

                            JSONObject sendData = new JSONObject();
                            sendData.put("session", sid);
                            sendData.put("wishes", array);


                            IWServer.getInstance().sendRequestDeleteWant(sendData, new IWCallback() {
                                @Override
                                public void call(Response data) {
                                    progressDialog.dismiss();
                                    if (data.code() == 508) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(IWExecutorsWantActivity.this);
                                                builder.setTitle("Ошибка");
                                                builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                                builder.setCancelable(false);
                                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        Intent intent = new Intent("dev.mabee.relog");
                                                        (IWExecutorsWantActivity.this).sendBroadcast(intent);
                                                    }
                                                });
                                                builder.create().show();
                                            }
                                        });
                                        return;
                                    } else {
                                        IWExecutorsWantActivity.this.finish();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    } else {
                        Intent intent = new Intent(IWExecutorsWantActivity.this, IWExecutorsWantDialog.class);
                        intent.putExtra("userID", it.getId());
                        intent.putExtra("uuid", it.uuid);
                        intent.putExtra("entity", it.company);
                        intent.putExtra("status", it.getType());
                        intent.putExtra("fio", it.getFio());
                        intent.putExtra("tags", it.getKeyword());
                        intent.putExtra("phone", it.getPhone());
                        intent.putExtra("phone", it.getPhone());
                        startActivityForResult(intent, 0);
                    }
                }
            }
        });

        TextView headerText = (TextView) findViewById(R.id.executorswant_screen_header);

        headerText.setText(hed);

        final String uuid = getIntent().getExtras().getString("uuid");
        SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        String session = preferences.getString(IWUserModel.sessionID, "none");

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синхронизация...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();


        IWServer.getInstance().sendRequestUsers(session, uuid, new IWCallback() {
            @Override
            public void call(Response data) {
                if (data.code() == 508) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            AlertDialog.Builder builder = new AlertDialog.Builder(IWExecutorsWantActivity.this);
                            builder.setTitle("Ошибка");
                            builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent("dev.mabee.relog");
                                    sendBroadcast(intent);
                                }
                            });
                            builder.create().show();

                        }
                    });
                    return;
                }
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(data.body().string());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String userID = jsonObject.getString("user_id");
                        String firstName = jsonObject.getString("first_name");
                        String second = jsonObject.getString("last_name");
                        String lastMane = jsonObject.getString("second_name");
                        String phone = jsonObject.getString("phone_number");
                        String already_voting = jsonObject.getString("already_voting");
                        String entity = null;
                        if (jsonObject.has("entity_name")) {
                            entity = jsonObject.getString("entity_name");
                        } else {
                            entity = "";
                        }
                        String tags = jsonObject.getString("tags");
                        String mark = jsonObject.getString("mark");

                        String lico = "null";
                        if (entity.isEmpty()) {
                            lico = "0";
                        } else {
                            lico = "1";
                        }

                        items.add(new IWAdapter.Item(lico, entity, second + " " + firstName + " " + lastMane, tags, "+7" + phone, mark, userID, uuid , already_voting ));
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (items.size() > 0) {
                                adapter.swapData(items);
                                textView.setVisibility(View.GONE);
                                listView.setVisibility(View.VISIBLE);
                            } else {
                                listView.setVisibility(View.GONE);
                                textView.setVisibility(View.VISIBLE);
                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 5) {
            finish();
            setResult(5);
        }
    }

    public void iwDeleteWantActivity_ClickBack(View view) {
        onBackPressed();
    }

    private static class IWAdapter extends BaseAdapter {

        ArrayList<Item> items = new ArrayList<Item>();

        public static class Item {
            public String getType() {
                return type;
            }

            public String getCompany() {
                return company;
            }

            public String getFio() {
                return fio;
            }

            public String getKeyword() {
                return keyword;
            }

            public String getPhone() {
                return phone;
            }

            public String getRating() {
                return rating;
            }

            public Item(String type, String company, String fio, String keyword, String phone, String rating, String id, String uuid , String voiting) {
                this.type = type;
                this.company = company;
                this.fio = fio;
                this.keyword = keyword;
                this.phone = phone;
                this.rating = rating;
                this.id = id;
                this.uuid = uuid;
                this.voiting = voiting;
            }

            private String type;
            private String company;
            private String fio;
            private String keyword;
            private String phone;
            private String rating;
            public String uuid;
            public String voiting;


            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            private String id;
        }

        private Context context;

        public IWAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        public void swapData(ArrayList<Item> list) {
            this.items = list;
            notifyDataSetChanged();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.executorswant_screen_row, parent, false);

            TextView textViewLico = (TextView) rowView.findViewById(R.id.executorswant_screen_row_lico);
            TextView textViewCName = (TextView) rowView.findViewById(R.id.executorswant_screen_row_companyname);
            TextView textViewFio = (TextView) rowView.findViewById(R.id.executorswant_screen_row_fio);
            TextView textViewTags = (TextView) rowView.findViewById(R.id.executorswant_screen_row_tags);
            TextView textViewPhone = (TextView) rowView.findViewById(R.id.executorswant_screen_row_phone);
            TextView textViewRating = (TextView) rowView.findViewById(R.id.executorswant_screen_row_ratingtv);
            LinearLayout ratingBar = (LinearLayout) rowView.findViewById(R.id.executorswant_screen_row_rating);


            Item item = items.get(position);
            textViewFio.setText(item.getFio());
            if (item.getType().equals("1")) {
                textViewLico.setText("Юридическое лицо");
                textViewCName.setText(item.getCompany());
            } else {
                textViewLico.setText("Физическое лицо (Частник)");
                textViewCName.setVisibility(View.GONE);
            }

            textViewTags.setText(item.getKeyword());
            textViewRating.setText(item.getRating());
            textViewPhone.setText(item.getPhone());

            int rate = Integer.parseInt(item.getRating());
            for (int i = 0; i < ratingBar.getChildCount(); i++) {
                if (i < rate) {
                    ((ImageView) ratingBar.getChildAt(i)).setImageResource(R.drawable.star_filled);
                } else {
                    break;
                }
            }
            return rowView;
        }
    }

    public static class IWExecutorsWantDialog extends FragmentActivity {
        String userID = "";
        String uuid = "";
        String fio = "";
        String entity = "";
        String status = "";
        String tags = "";
        String phons = "";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.executorswant_screen_dialog);


            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
                setFinishOnTouchOutside(false);
            }

            SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
            final String session  = preferences.getString(IWUserModel.sessionID, "none");
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                userID = bundle.getString("userID");
                uuid = bundle.getString("uuid");
                fio = bundle.getString("fio");
                entity = bundle.getString("entity");
                status = bundle.getString("status");
                tags = bundle.getString("tags");
                phons = bundle.getString("phone");
            }


            TextView textFio = (TextView) findViewById(R.id.executorswant_screen_dialog_fio);
            TextView textEnt = (TextView) findViewById(R.id.executorswant_screen_dialog_ent);
            TextView textFizur = (TextView) findViewById(R.id.executorswant_screen_dialog_fizur);
            TextView textTags = (TextView) findViewById(R.id.executorswant_screen_dialog_tags);
            TextView textPhone = (TextView) findViewById(R.id.executorswant_screen_dialog_phone);

            textPhone.setText(phons);
            textPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    String str = "tel:"+phons;
                    intent.setData(Uri.parse(str));
                    startActivity(intent);
                }
            });
            textFio.setText(fio);
            if (entity != null) {
                textEnt.setText(entity);
            }

            if (status.equals("2")) {
                textFizur.setText("Юридическое лицо");
            } else {
                textFizur.setText("Физическое лицо");
            }

            textTags.setText(tags);


            RelativeLayout goodButton = (RelativeLayout) findViewById(R.id.executorswant_screen_dialog_good);
            goodButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final ProgressDialog dialog = new ProgressDialog(IWExecutorsWantDialog.this);
                    dialog.setMessage("Синхронизация");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();

                    IWServer.getInstance().sendRequestMark(session, uuid, "1", userID, new IWCallback() {
                        @Override
                        public void call(Response data) {
                            dialog.dismiss();
                            setResult(5);
                            IWExecutorsWantDialog.this.finish();
                        }
                    });
                }
            });

            RelativeLayout badButton = (RelativeLayout) findViewById(R.id.executorswant_screen_dialog_bad);
            badButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final ProgressDialog dialog = new ProgressDialog(IWExecutorsWantDialog.this);
                    dialog.setMessage("Синхронизация");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();

                    IWServer.getInstance().sendRequestMark(session, uuid, "-1", userID, new IWCallback() {
                        @Override
                        public void call(Response data) {
                            if (data.code()==508) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(IWExecutorsWantDialog.this);
                                builder.setTitle("Ошибка");
                                builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                builder.setCancelable(false);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent("dev.mabee.relog");
                                        sendBroadcast(intent);
                                    }
                                });
                                builder.create().show();
                                return;
                            }
                            dialog.dismiss();
                            setResult(5);
                            IWExecutorsWantDialog.this.finish();
                        }
                    });


                }
            });

        }
    }

}
