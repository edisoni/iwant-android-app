package dev.mabee.iwant;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

/**
 * Created by Ed on 31.10.2014.
 */


public class IWEditActivity extends Activity {


    RadioButton radioButtonFiz;
    RadioButton radioButtonUr;
    EditText textEntity;
    EditText textFamily;
    EditText textName;
    EditText textNumber;
    EditText textOtch;

    TextView textView;
    TextView textRegion;
    TextView textCity;

    String cityId = "";
    String session = "none";

    BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_screen);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.relog"));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        textEntity = (EditText) findViewById(R.id.edit_screen_eb_entity);
        textFamily = (EditText) findViewById(R.id.edit_screen_eb_family);
        textName = (EditText) findViewById(R.id.edit_screen_eb_name);
        textNumber = (EditText) findViewById(R.id.edit_screen_eb_number);
        textOtch = (EditText) findViewById(R.id.edit_screen_eb_otchname);

        textCity = (TextView) findViewById(R.id.edit_screen_tv_town);
        textRegion = (TextView) findViewById(R.id.edit_screen_tv_region);

        final RelativeLayout entLay = (RelativeLayout) findViewById(R.id.edit_screen_rl_entity);
        radioButtonFiz = (RadioButton) findViewById(R.id.edit_screen_rb_fiz_face);
        radioButtonFiz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    entLay.setVisibility(View.GONE);
                } else {
                    entLay.setVisibility(View.VISIBLE);
                }
            }
        });
        radioButtonUr = (RadioButton) findViewById(R.id.edit_screen_rb_yr_face);
        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.edit_screen_ln_town);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IWEditActivity.this, IWSelectTown.class);
                startActivityForResult(intent, 5);
            }
        });

        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radiogroup);




        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синхронизация...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        SharedPreferences pref = getSharedPreferences("iwant_data", MODE_PRIVATE);
        String session = pref.getString(IWUserModel.sessionID, "none");


        IWServer.getInstance().sendRequestGetExpired(session,new IWCallback() {
            @Override
            public void call(Response data) {
                progressDialog.cancel();
                try {
                    JSONObject jsonObject = new JSONObject(data.body().string());
                    String days = jsonObject.getString("days");

                    if (days.equals("0")) {
                        IWEditActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textEntity.setEnabled(true);
                                textFamily.setEnabled(true);
                                textName.setEnabled(true);
                                textOtch.setEnabled(true);
                                textCity.setEnabled(true);
                                linearLayout.setClickable(true);
                                textRegion.setEnabled(true);
                                radioButtonFiz.setEnabled(true);
                                radioButtonUr.setEnabled(true);

                                textNumber.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                                textName.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                                textFamily.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                                textEntity.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                                textOtch.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                                linearLayout.setBackgroundColor(Color.TRANSPARENT);
                                radioGroup.setBackgroundColor(Color.TRANSPARENT);
                            }
                        });

                    } else {
                        IWEditActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textEntity.setEnabled(false);
                                textFamily.setEnabled(false);
                                textName.setEnabled(false);
                                textOtch.setEnabled(false);
                                textCity.setEnabled(false);
                                linearLayout.setClickable(false);
                                textRegion.setEnabled(false);

                                textNumber.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                                textName.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                                textFamily.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                                textEntity.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                                textOtch.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                                linearLayout.setBackgroundColor(getResources().getColor(R.color.disabled_color));
                                radioGroup.setBackgroundColor(getResources().getColor(R.color.disabled_color));

                                radioButtonFiz.setEnabled(false);
                                radioButtonUr.setEnabled(false);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });



        SharedPreferences prefs = getSharedPreferences("iwant_data", MODE_PRIVATE);
        String firstName = prefs.getString(IWUserModel.userFirstName, "none");
        String secondName = prefs.getString(IWUserModel.userLastName, "none");
        String lastName = prefs.getString(IWUserModel.userSecondName, "none");
        String entity = prefs.getString(IWUserModel.userEntityName, "none");
        String phone = prefs.getString(IWUserModel.userPhone, "none");
        String address = prefs.getString(IWUserModel.userCity, "none");
        String region = prefs.getString(IWUserModel.userRegion, "none");
        cityId = prefs.getString(IWUserModel.userCityID, "");
        String fizur = prefs.getString(IWUserModel.userStatus, "none");

        Log.i("mLogs"," Region on load : " +  region);

        textName.setText(firstName);
        textFamily.setText(secondName);
        textOtch.setText(lastName);


        textNumber.setText(phone.replace("+7", ""));
        textCity.setText(address);
        textRegion.setText(region);

        if (fizur.equals("1")) {
            radioButtonUr.setChecked(false);
            radioButtonFiz.setChecked(true);
            entLay.setVisibility(View.GONE);
        } else {
            radioButtonFiz.setChecked(false);
            radioButtonUr.setChecked(true);
            entLay.setVisibility(View.VISIBLE);
            textEntity.setText(entity);
        }


        final TextView txtDlt = (TextView) findViewById(R.id.edit_screen_dlt);
        TextView txtLogout = (TextView) findViewById(R.id.edit_screen_exit);


        txtDlt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(IWEditActivity.this);
                alertBuilder.setMessage("Вы дейстивительно хотите удалить профиль?");
                alertBuilder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences prefs = getSharedPreferences("iwant_data", MODE_PRIVATE);
                        String session = prefs.getString(IWUserModel.sessionID, "none");

                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.commit();

                        final ProgressDialog progressDialog = new ProgressDialog(IWEditActivity.this);
                        progressDialog.setMessage("Синхронизация");
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.setCancelable(false);
                        progressDialog.show();


                        IWServer.getInstance().sendRequestDltUser(session, new IWCallback() {
                            @Override
                            public void call(Response data) {
                                progressDialog.dismiss();
                                if (data.code() == 200) {
                                    Intent intent = new Intent("dev.mabee.relog");
                                    sendBroadcast(intent);
                                } else {
                                    IWEditActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWEditActivity.this);
                                            alertDialog.setMessage("Ошибка на сервере, попробуйте еще раз.");
                                            alertDialog.setPositiveButton("OK", null);
                                            alertDialog.create().show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                alertBuilder.setNegativeButton("Нет", null);
                alertBuilder.create().show();
            }
        });

        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(IWEditActivity.this);
                alertBuilder.setMessage("Вы дейстивительно хотите выйти?");
                alertBuilder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences.Editor editor = getSharedPreferences("iwant_data", MODE_PRIVATE).edit();
                        editor.clear();
                        editor.commit();
                        Intent intent = new Intent("dev.mabee.relog");
                        sendBroadcast(intent);
                    }
                });
                alertBuilder.setNegativeButton("Нет", null);
                alertBuilder.create().show();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {}
    }

    public void iwRegActivity_ClickOk(View view) {

        if (textFamily.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWEditActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения ФИО");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (textName.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWEditActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения ФИО");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (textOtch.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWEditActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения ФИО");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (!radioButtonFiz.isChecked()) {
            if (textEntity.getText().toString().isEmpty()) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWEditActivity.this);
                alertDialog.setMessage("Проверьте правильность заполнения наименования организации");
                alertDialog.setTitle("Ошибка");
                alertDialog.setPositiveButton("ОК", null);
                alertDialog.create().show();
                return;
            }
        }
        if (textNumber.getText().toString().length() < 10 || textNumber.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWEditActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения номера телефона");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (cityId.isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWEditActivity.this);
            alertDialog.setMessage("Выберите населенный пункт");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }


        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Синхронизация...");
        dialog.show();

        String family = textFamily.getText().toString();
        String name = textName.getText().toString();
        String number = textNumber.getText().toString();
        String otch = textOtch.getText().toString();
        String entity = textEntity.getText().toString();
        String status = radioButtonFiz.isChecked() ? "1" : "2";

        SharedPreferences.Editor preferences = getSharedPreferences("iwant_data", MODE_PRIVATE).edit();
        preferences.putString(IWUserModel.userCity, textCity.getText().toString());
        preferences.putString(IWUserModel.userEntityName, entity);
        preferences.putString(IWUserModel.userFirstName, name);
        preferences.putString(IWUserModel.userLastName, family);
        preferences.putString(IWUserModel.userSecondName, otch);
        preferences.putString(IWUserModel.userPhone, number);
        preferences.putString(IWUserModel.userStatus, status);
        preferences.putString(IWUserModel.userRegion, textRegion.getText().toString());
        preferences.putString(IWUserModel.userCityID, cityId);
        preferences.commit();


        SharedPreferences prefs = getSharedPreferences("iwant_data", MODE_PRIVATE);
        session = prefs.getString(IWUserModel.sessionID, "none");
        JSONObject object = IWUserModel.createJEObject(session, name,
                otch,
                family,
                number,
                cityId,
                status,
                entity);


        Log.i("mLogs", "Status : " + status);
        IWServer.getInstance().sendRequestEditProfile(object, new IWCallback() {
            @Override
            public void call(Response data) {
                if (data.code() == 508) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            AlertDialog.Builder builder = new AlertDialog.Builder(IWEditActivity.this);
                            builder.setTitle("Ошибка");
                            builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent("dev.mabee.relog");
                                    sendBroadcast(intent);
                                }
                            });
                            builder.create().show();

                        }
                    });
                    return;
                }
                try {
                    Log.i("mLogs", "Code : " + data.code());
                    Log.i("mLogs", "Message : " + data.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
                IWEditActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                });

            }
        });


    }
    // Ставрополь

    public void iwRegActivity_ClickCancel(View view) {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 5) {
            cityId = data.getExtras().getString("id");
            String town = data.getExtras().getString("town");
            String region = data.getExtras().getString("region");


            Log.i("mLogs"," Region on Select town : " + region);
            TextView textView = (TextView) findViewById(R.id.edit_screen_tv_town);
            textView.setText(town);
            TextView textViewr = (TextView) findViewById(R.id.edit_screen_tv_region);
            textViewr.setText(region);
        }

    }

}
