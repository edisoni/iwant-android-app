package dev.mabee.iwant;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Edisoni on 08.12.2014.
 */
public class IWSelectTown extends FragmentActivity {
    Timer[] timers;

    ArrayList<IWAdapter.Item> items = new ArrayList<>();
    IWAdapter adapter;
    int currentpage = 0;


    public void iwSelectTown_ClickBack(View view) {
        onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selecttown_screen);

        final EditText editText = (EditText) findViewById(R.id.selecttown_edittext);
        final PullToRefreshListView listView = (PullToRefreshListView) findViewById(R.id.selecttown_listview);
        adapter = new IWAdapter(this);
        timers = new Timer[1];

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                IWAdapter.Item item = (IWAdapter.Item) adapter.getItem(i-1);

                Intent data  = new Intent();
                data.putExtra("id",item.id);
                data.putExtra("town",item.city);
                data.putExtra("region",item.region);

                setResult(5, data);
                finish();
            }
        });
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                Log.i("mLogs","REFRESH");
                final String text = editText.getText().toString();
                currentpage += 1;
                IWServer.getInstance().sendRequestTowns(text, "" + currentpage, new IWCallback() {
                    @Override
                    public void call(Response data) {
                        Log.i("mLogs","Code : " + data.code());
                        try {
                            JSONArray jsonArray = new JSONArray(data.body().string());
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                items.add(new IWAdapter.Item(object.getString("id"), object.getString("city"), object.getString("region")));
                            }
                            IWSelectTown.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    adapter.swapData(items);
                                    listView.onRefreshComplete();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String text = editText.getText().toString();
                if (text.contains(" ")) {
                    text = text.replaceAll(" ","");
                    editText.setText(text);
                    editText.setSelection(text.length());
                }

                if (timers[0] != null) {
                    timers[0].cancel();
                }
                timers[0] = new Timer();
                timers[0].schedule(new TimerTask() {
                    @Override
                    public void run() {
                        IWSelectTown.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                items.clear();
                                final ProgressDialog progressDialog = new ProgressDialog(IWSelectTown.this);
                                progressDialog.setCanceledOnTouchOutside(false);
                                progressDialog.setMessage("Получение городов");
                                progressDialog.show();

                                final String text = editText.getText().toString();
                                IWServer.getInstance().sendRequestTowns(text, "0", new IWCallback() {
                                    @Override
                                    public void call(Response data) {
                                        progressDialog.cancel();
                                        Log.i("mLogs","Code : " + data.code());
                                        try {
                                            JSONArray jsonArray = new JSONArray(data.body().string());
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject object = jsonArray.getJSONObject(i);
                                                items.add(new IWAdapter.Item(object.getString("id"), object.getString("city"), object.getString("region")));
                                            }
                                            IWSelectTown.this.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    adapter.swapData(items);
                                                }
                                            });

                                        currentpage = 0;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                            }
                        });
                    }

                }, 1500);

            }
        });
    }

    private static class IWAdapter extends BaseAdapter {

        ArrayList<Item> items = new ArrayList<Item>();

        public static class Item {

            public String id;
            public String city;
            public String region;

            public Item(String id, String city,String region) {
                this.id = id;
                this.city = city;
                this.region  = region;
            }
        }

        private Context context;

        public IWAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        public void swapData(ArrayList<Item> list) {
            this.items = list;
            notifyDataSetChanged();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(R.layout.selecttown_row, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.selecttown_row_textview);
            textView.setText(items.get(position).city);
            TextView textView1 = (TextView) rowView.findViewById(R.id.selecttown_row_textview1);
            textView1.setText(items.get(position).region);

            return rowView;
        }
    }

}

