package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ed on 01.12.2014.
 */
public class IWAcceptWant extends FragmentActivity {

    String uuid;
    String active;
    String city;
    String region;
    String time;
    String wishStatus;
    String session;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acceptwant_screen);
        Bundle bundle = getIntent().getExtras();

        SharedPreferences preferences = getSharedPreferences("iwant_data",MODE_PRIVATE);
        session = preferences.getString(IWUserModel.sessionID, "none");

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.acceptwant_screen_buttons);
        final LinearLayout linearLayoutSorry = (LinearLayout) findViewById(R.id.acceptwant_screen_sorry);
        if (bundle!=null) {
            TextView textView = (TextView) findViewById(R.id.acceptwant_screen_textview);
            textView.setText(bundle.getString("desc"));
            uuid   =  bundle.getString("uuid");
            active =  bundle.getString("active");
            if (active.equals("0")) {
                linearLayout.setVisibility(View.GONE);
                linearLayoutSorry.setVisibility(View.VISIBLE);
            } else {
                linearLayout.setVisibility(View.VISIBLE);
                linearLayoutSorry.setVisibility(View.GONE);
            }
            city   =  bundle.getString("city");
            region =  bundle.getString("region");
            time   =  bundle.getString("time");
            wishStatus = bundle.getString("wishStatus");
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Синронизация...");
            progressDialog.setCancelable(false);
            progressDialog.show();


            IWServer.getInstance().sendRequestGetStatusWant(session, uuid, new IWCallback() {
                @Override
                public void call(Response data) {
                    try {
                        JSONObject jsonObject = new JSONObject(data.body().string());
                        final String sts = jsonObject.getString("status");
                        Log.i("mLogs"," STATUS : " + sts);
                        IWAcceptWant.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                if (sts.equals("1")) {
                                    linearLayout.setVisibility(View.VISIBLE);
                                    linearLayoutSorry.setVisibility(View.GONE);
                                } else {

                                    linearLayout.setVisibility(View.GONE);
                                    linearLayoutSorry.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }



        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.login"));
    }
    public void iwAcceptWant_ClickBack(View view) {
        setResult(10);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        IWServer.getInstance().sendRequestRejectWant(session, uuid, null);
        super.onBackPressed();
    }

    public void iwAcceptWant_ClickReject(View view) {
        IWServer.getInstance().sendRequestRejectWant(session, uuid, null);
        setResult(10);
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {}
    }

    public void iwAcceptWant_ClickBuy(View view) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синронизация...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        IWServer.getInstance().sendRequestGetPrice(session, wishStatus, new IWCallback() {



            @Override
            public void call(Response data) {
                progressDialog.dismiss();
                if (data.code()==508) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            AlertDialog.Builder builder = new AlertDialog.Builder(IWAcceptWant.this);
                            builder.setTitle("Ошибка");
                            builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent("dev.mabee.relog");
                                    sendBroadcast(intent);
                                }
                            });
                            builder.create().show();
                        }
                    });
                    return;
                }

                try {
                    String dd = data.body().string();
                    JSONObject jsonObject = new JSONObject(dd);
                    Intent intent =  new Intent(IWAcceptWant.this,IWPayActivity.class);
                    intent.putExtra("uuid",uuid);
                    if (jsonObject.length()>1) {
                        intent.putExtra("free", true);
                    } else {
                        String SKU_FAKE  = jsonObject.getString("originalPriceId");
                        intent.putExtra("free", false);
                        intent.putExtra("SKU", SKU_FAKE);



                        Log.i("mLogs", "SKU : " + SKU_FAKE);
                    }
                    startActivityForResult(intent,0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==5) {
            finish();
        }
    }
}
