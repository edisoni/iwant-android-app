package dev.mabee.iwant;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.util.List;


public class IWGcmReceiver extends BroadcastReceiver {


    NotificationManager manager;
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i("mLogs"," GCM RECEIVED");
        Bundle data = intent.getExtras();
        String type = data.getString("type");
        this.context = context;
        if (type!=null) {
            if (type.equals("newWishes") && !isAppForground(context)) {
                newOrder(data.getString("text"));
            }
            if (type.equals("realtime")) {
                realTime(data.getString("uuid"));
            }
            if (type.equals("subscribeEnd")) {
                subscribe(data.getString("text"));
            }
        }
    }
    public boolean isAppForground(Context mContext) {

        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(mContext.getPackageName())) {
                return false;
            }
        }

        return true;
    }
    public void newOrder(String text) {



        Intent i = new Intent(context,IWStartActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("FLAG","YES");

        //Define sound URI
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification noti = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setContentTitle("Я хочу")
                .setContentText(text)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setAutoCancel(true).build();
        manager.notify(0,noti);

        SharedPreferences preferences = context.getSharedPreferences("iwant_data", Context.MODE_PRIVATE);
        String token = preferences.getString("registration_id", "");

        IWServer.getInstance().sendRequestNotify(token, null);
    }

    public void realTime(String uuid) {
        Log.i("mLogs","REAL TIME");
        Intent intent = new Intent("iw.orders");
        intent.putExtra("uuid",uuid);
        context.sendBroadcast(intent);
    }

    public void subscribe(String text) {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification noti = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setContentTitle("Я хочу")
                .setContentText(text)
                .setSound(soundUri)
                .setVibrate(new long[] {1000,1000,1000,1000,1000})
                .setAutoCancel(true).build();
        manager.notify(0,noti);
    }
}
