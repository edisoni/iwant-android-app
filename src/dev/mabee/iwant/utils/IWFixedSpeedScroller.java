package dev.mabee.iwant.utils;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * Created by Ed on 18.11.2014.
 */
public class IWFixedSpeedScroller extends Scroller {

    private int mDuration = 1000;

    public IWFixedSpeedScroller(Context context) {
        super(context);
    }
    public IWFixedSpeedScroller(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }
    public IWFixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) { super(context, interpolator, flywheel);}

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, mDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        super.startScroll(startX, startY, dx, dy, mDuration);
    }
}

