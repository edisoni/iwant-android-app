package dev.mabee.iwant.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Edison4 on 04.02.2015.
 */
public class UnderlinedEditText extends EditText {
    private Rect mRect;
    private Paint mPaint;
    int widthMsSize;
    int heightMsSize;


    public UnderlinedEditText(Context context) {
        super(context);

    }

    public UnderlinedEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    // we need this constructor for LayoutInflater
    public UnderlinedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(5);
        mPaint.setColor(Color.GREEN);
    }

    protected void onMeasure(final int widthMeasureSpec,final int heightMeasureSpec) {
        widthMsSize = MeasureSpec.getSize(widthMeasureSpec);
        heightMsSize = MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(widthMsSize,heightMsSize);
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawRect(0,0, getRight() , getBottom(), mPaint);
        super.onDraw(canvas);
    }
}
