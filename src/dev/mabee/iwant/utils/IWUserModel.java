package dev.mabee.iwant.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ed on 06.11.2014.
 */
public class IWUserModel  {

    final public static String userFirstName  = "userFirstName";
    final public static String userLastName   = "userLastName";
    final public static String userSecondName = "userSecondName";
    final public static String userPhone      = "userPhone";
    final public static String userRegion     = "userRegion";
    final public static String userCity       = "userCity";
    final public static String userCityID     = "userCityID";
    final public static String userFederalDistrict = "userFederalDistrict";
    final public static String userStatus     = "userStatus";
    final public static String userEntityName = "userEntityName";
    final public static String userAdvSource  = "advSource";
    final public static String genUID = "genUid";
    final public static String sessionID = "sid";


    public static JSONObject createJObject(String FirstName,String LastName,String SecondName,String Phone,String cityID,String Status,String entity, String AdvSource) {
        JSONObject object = new JSONObject();

        try {
            object.put(userAdvSource,userAdvSource);
            object.put(userFirstName,FirstName);
            object.put(userLastName,LastName);
            object.put(userSecondName,SecondName);
            object.put(userPhone, Phone);
            object.put(userCityID,cityID);
            object.put(userStatus,Status);
            object.put(userEntityName,entity);

            return object;
        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject createJEObject(String session,String first, String last, String second, String phone, String city, String status, String entity) {
        JSONObject userData = new JSONObject();

        try {
            userData.put(userFirstName,first);
            userData.put(userLastName,second);
            userData.put(userSecondName,last);
            userData.put(userCityID,city);
            userData.put(userStatus,status);
            userData.put(userEntityName,entity);


            JSONObject object = new JSONObject();
            object.put("userData",userData);
            object.put("session",session);

            return object;
        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject createJObject(String FirstName,String LastName,String SecondName,String Phone,String cityID,String Status,String entity) {
        JSONObject object = new JSONObject();

        try {
            object.put(userFirstName,FirstName);
            object.put(userLastName,LastName);
            object.put(userSecondName,SecondName);
            object.put(userPhone, Phone);
            object.put(userCityID,cityID);
            object.put(userStatus,Status);
            object.put(userEntityName,entity);

            return object;
        } catch (JSONException e) {
            return null;
        }
    }

}
