package dev.mabee.iwant.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Ed on 18.11.2014.
 */
public class IWViewPager extends ViewPager {

    public IWViewPager(Context context) {
        super(context);
    }

    public IWViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return  false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

}