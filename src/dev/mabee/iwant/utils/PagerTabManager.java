package dev.mabee.iwant.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ed on 28.11.2014.
 */
public class PagerTabManager implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

    private final FragmentActivity mActivity;
    private final TabHost mTabHost;
    private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
    private final ViewPager mViewPager;
    private final PagerAdapter mViewPagerAdapter;
    TabInfo mLastTab;

    static final class TabInfo {
        private final String tag;
        private final Class<?> clss;
        private final Bundle args;
        private Fragment fragment;

        TabInfo(String _tag, Class<?> _class, Bundle _args) {
            tag = _tag;
            clss = _class;
            args = _args;
        }
    }

    static class DummyTabFactory implements TabHost.TabContentFactory {
        private final Context mContext;

        public DummyTabFactory(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }

    /**
     * Simple Pager Adapter that contains a {@link List} of {@link Fragment}.
     */
    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragments = new ArrayList<Fragment>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        public void addFragment(final Fragment fragment) {
            mFragments.add(fragment);
        }
    }

    public PagerTabManager(FragmentActivity activity, TabHost tabHost, ViewPager viewPager) {
        mActivity = activity;
        mTabHost = tabHost;
        mViewPagerAdapter = new PagerAdapter(activity.getSupportFragmentManager());
        mViewPager = viewPager;
        mViewPager.setOnPageChangeListener(this);
        mViewPager.setAdapter(mViewPagerAdapter);
        mTabHost.setOnTabChangedListener(this);
    }

    public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
        tabSpec.setContent(new DummyTabFactory(mActivity));
        String tag = tabSpec.getTag();
        TabInfo info = new TabInfo(tag, clss, args);

        mViewPagerAdapter.addFragment(Fragment.instantiate(mActivity, info.clss.getName(), args));
        mTabs.put(info.tag, info);
        mTabHost.addTab(tabSpec);
    }

    @Override
    public void onTabChanged(String tabId) {
        int position = mTabHost.getCurrentTab();
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int position) {
        mTabHost.setCurrentTab(position);
    }
}