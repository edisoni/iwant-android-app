package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.*;
import android.widget.*;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import dev.mabee.iwant.utils.IWWantModel;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ed on 10.11.2014.
 */
public class IWWants extends FragmentActivity {

    private TabHost mTabHost;
    private TabsAdapter adapter;
    public TextView titleView;
    BroadcastReceiver broadcastReceiver;
    private static int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wants_screen);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("dev.mabee.relog"));

        titleView = (TextView) findViewById(R.id.wants_screen_title);
        mTabHost = (TabHost) findViewById(R.id.tabhost);
        mTabHost.setup();





        ViewPager mViewPager = (ViewPager) findViewById(R.id.wviewpager);
        adapter = new TabsAdapter(this, mTabHost, mViewPager);


        adapter.addTab(mTabHost.newTabSpec("wants").setIndicator(createTabView(this, "Желания")), IWFragmentWts.class, null);
        adapter.addTab(mTabHost.newTabSpec("others").setIndicator(createTabView(this, "Прочие")), IWFragmentOther.class, null);



        for(int i = 0;i<2;i++) {
            ViewGroup vg = (ViewGroup) mTabHost.getTabWidget().getChildAt(i);
            TextView textView = (TextView) vg.getChildAt(0);
            textView.setTextColor(Color.WHITE);
            textView.setTextSize(16.0f);
        }
    }


    private static View createTabView(final Context context, final String text) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        tv.setText(text);
        return view;
    }


    public void iwWantsActivity_ClickAddWant(View view) {
        if (count < 5) {
            Intent intent = new Intent(this, IWAddWant.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {}
    }

    public void iwWantsActivity_ClickBack(View view) {
        onBackPressed();
    }

    public static class IWFragmentWts extends Fragment {

        IWAdapter adapter;
        ListView listView;
        LinearLayout linearLayout;

        static TextView wtfButton;
        static TextView addWantButton;
        BroadcastReceiver receiver;
        @Override
        public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.wants_screen_fragment_wts, container, false);





            adapter = new IWAdapter(getActivity());
            wtfButton = (TextView) v.findViewById(R.id.wants_screen_fragment_wts_wtf);
            wtfButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), IWWantsDialog.class);
                    startActivity(intent);
                }
            });
            addWantButton = (TextView) v.findViewById(R.id.wants_screen_fragment_wts_addwant);
            listView = (ListView) v.findViewById(R.id.wants_screen_fragment_wts_listview);
            linearLayout = (LinearLayout) v.findViewById(R.id.wants_screen_fragment_wts_notwants);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    IWAdapter.Item want = (IWAdapter.Item) adapter.getItem(position);
                    Intent intent = new Intent(getActivity(), IWExecutorsWantActivity.class);
                    intent.putExtra("uuid", want.uuid);
                    intent.putExtra("HEADER", want.getText());
                    startActivity(intent);
                }
            });


            IntentFilter filter = new IntentFilter();
            filter.addAction("dev.mabee.sync");
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    final ProgressDialog dialog = new ProgressDialog(getActivity());
                    dialog.setMessage("Синхронизация");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    sync(dialog);
                }
            };
            getActivity().registerReceiver(receiver, filter);
            return v;
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            try {
                getActivity().unregisterReceiver(receiver);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void onResume() {
            super.onResume();
            final ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Синхронизация");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            sync(dialog);
        }

        public void sync(final ProgressDialog dialog) {

            final ArrayList<IWAdapter.Item> items = new ArrayList<IWAdapter.Item>();

            SharedPreferences preferences = getActivity().getSharedPreferences("iwant_data", MODE_PRIVATE);
            String session = preferences.getString(IWUserModel.sessionID, "none");
            IWServer.getInstance().sendRequestWants(session, new IWCallback() {
                @Override
                public void call(Response data) {
                    dialog.dismiss();

                    if (data.code() == 508) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Ошибка");
                                builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                builder.setCancelable(false);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent("dev.mabee.relog");
                                        getActivity().sendBroadcast(intent);
                                    }
                                });
                                builder.create().show();
                            }
                        });
                        return;
                    }
                    try {

                        String resp = data.body().string();
                        if (resp.length() > 0) {
                            JSONObject objectWants = new JSONObject(resp);
                            if (objectWants.isNull("wishes")) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        linearLayout.setVisibility(View.VISIBLE);
                                        listView.setVisibility(View.GONE);
                                        adapter.swapData(items);
                                    }
                                });
                                return;
                            }
                            JSONArray array = objectWants.getJSONArray("wishes");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject objectWant = array.getJSONObject(i);
                                String text = objectWant.getString(IWWantModel.text);
                                boolean haveExe = objectWant.getString(IWWantModel.haveExecturos).equals("0");
                                String callTimeEnd = objectWant.getString(IWWantModel.callTimeEnd);
                                String callTimeStart = objectWant.getString(IWWantModel.callTimeStart);
                                String lim = objectWant.getString(IWWantModel.executorLimit);
                                String stat = objectWant.getString(IWWantModel.executorStatus);
                                String uuid = objectWant.getString(IWWantModel.uuid);

                                items.add(new IWAdapter.Item(text, haveExe, lim, stat, callTimeStart, callTimeEnd, uuid));
                            }
                            if (items.size() > 0) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        count = items.size();
                                        if (count >= 5) {
                                            wtfButton.setVisibility(View.VISIBLE);
                                            addWantButton.setVisibility(View.GONE);
                                        } else {
                                            wtfButton.setVisibility(View.GONE);
                                            addWantButton.setVisibility(View.VISIBLE);
                                        }
                                        linearLayout.setVisibility(View.GONE);
                                        listView.setVisibility(View.VISIBLE);
                                        adapter.swapData(items);
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (resultCode == 5) {
                if (count < 5) {
                    wtfButton.setVisibility(View.GONE);
                    addWantButton.setVisibility(View.VISIBLE);
                }
                final ProgressDialog dialog = new ProgressDialog(getActivity());
                dialog.setMessage("Синхронизация");
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                sync(dialog);
            }
        }

        private static class IWAdapter extends BaseAdapter {

            ArrayList<Item> items = new ArrayList<Item>();

            public static class Item {

                public String getText() {
                    return text;
                }

                private String text;
                private boolean haveExecutors;

                private String uuid;

                public Item(String text, boolean haveExecutors, String limitExecutors, String executorStatus, String callStart, String callEnd, String uuid) {
                    this.text = text;
                    this.haveExecutors = haveExecutors;
                    this.uuid = uuid;
                }
            }

            private Context context;

            public IWAdapter(Context context) {
                this.context = context;
            }

            @Override
            public int getCount() {
                return items.size();
            }

            public void swapData(ArrayList<Item> list) {
                this.items = list;
                notifyDataSetChanged();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rowView = inflater.inflate(R.layout.wants_screen_fragment_wts_row, parent, false);
                final ImageView imageBell = (ImageView) rowView.findViewById(R.id.wants_screen_fragment_wts_bell);
                final ImageView imageDell = (ImageView) rowView.findViewById(R.id.wants_screen_fragment_wts_delete);
                final TextView textView = (TextView) rowView.findViewById(R.id.wants_screen_fragment_wts_textview);

                final Item current = items.get(position);
                textView.setText(current.getText());

                if (current.haveExecutors) {
                    imageBell.setImageResource(R.drawable.white_circle);
                } else {
                    imageBell.setImageResource(R.drawable.bell);
                }

                imageDell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!current.haveExecutors) {
                            Intent intent = new Intent(context, IWExecutorsWantActivity.class);
                            intent.putExtra("uuid", current.uuid);
                            intent.putExtra("del", true);
                            intent.putExtra("HEADER", "Выберите исполнителя выполнившего работы");
                            context.startActivity(intent);
                        } else {
                            final ProgressDialog progressDialog = new ProgressDialog(context);
                            progressDialog.setMessage("Синхронизация");
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.show();

                            final SharedPreferences preferences = context.getSharedPreferences("iwant_data", MODE_PRIVATE);
                            String sid = preferences.getString(IWUserModel.sessionID, "none");

                            try {
                                JSONArray array = new JSONArray();
                                array.put(items.get(position).uuid);

                                JSONObject sendData = new JSONObject();
                                sendData.put("session", sid);
                                sendData.put("wishes", array);


                                IWServer.getInstance().sendRequestDeleteWant(sendData, new IWCallback() {
                                    @Override
                                    public void call(Response data) {
                                        progressDialog.dismiss();
                                        if (data.code() == 508) {
                                            ((IWWants)context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    AlertDialog.Builder builder = new AlertDialog.Builder( ((IWWants)context));
                                                    builder.setTitle("Ошибка");
                                                    builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                                    builder.setCancelable(false);
                                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            Intent intent = new Intent("dev.mabee.relog");
                                                            ((IWWants)context).sendBroadcast(intent);
                                                        }
                                                    });
                                                    builder.create().show();
                                                }
                                            });
                                            return;
                                        }

                                        if (data.code() == 200) {
                                            if (count < 5) {
                                                wtfButton.setVisibility(View.GONE);
                                                addWantButton.setVisibility(View.VISIBLE);
                                            }
                                            Intent local = new Intent();
                                            local.setAction("dev.mabee.sync");
                                            context.sendBroadcast(local);
                                        }
                                    }
                                });
                            } catch (Exception e) {

                            }
                        }
                    }
                });
                return rowView;
            }
        }

    }

    public static class IWFragmentOther extends Fragment {


        LinearLayout linearLayout;

        @Override
        public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.wants_screen_fragment_others, container, false);
            linearLayout = (LinearLayout) view.findViewById(R.id.wants_screen_fragment_others);


            IWServer.getInstance().sendRequestOtherTags(new IWCallback() {
                @Override
                public void call(Response data) {
                    try {
                        JSONArray array = new JSONArray(data.body().string());
                        final String[] strings = new String[array.length()];

                        for (int i = 0; i < array.length(); i++) {
                            strings[i] = array.getString(i);
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                View[] views = new View[strings.length];
                                for (int i = 0; i < strings.length; i++) {
                                    views[i] = create(strings[i], inflater);
                                }
                                populateViews(linearLayout, views, getActivity());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            return view;
        }

        public View create(String text, LayoutInflater layoutInflater) {
            View view = layoutInflater.inflate(R.layout.wants_screen_tag, null);
            ((TextView) view.findViewById(R.id.wants_screen_fragment_others_text)).setText(text);
            return view;
        }

        private void populateViews(LinearLayout linearLayout, View[] views, Context context) {
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            linearLayout.removeAllViews();
            int maxWidth = display.getWidth();

            linearLayout.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams params;
            LinearLayout newLL = new LinearLayout(context);
            newLL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            newLL.setGravity(Gravity.CENTER);
            newLL.setOrientation(LinearLayout.HORIZONTAL);

            int widthSoFar = 0;

            for (int i = 0; i < views.length; i++) {
                LinearLayout LL = new LinearLayout(context);
                LL.setOrientation(LinearLayout.HORIZONTAL);
                LL.setGravity(Gravity.LEFT);
                LL.setLayoutParams(new ListView.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                views[i].measure(0, 0);
                params = new LinearLayout.LayoutParams(views[i].getMeasuredWidth(), LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(20, 30, 0, 0);

                LL.addView(views[i], params);
                LL.measure(0, 0);
                widthSoFar += views[i].getMeasuredWidth() + 40;

                if (widthSoFar >= maxWidth) {
                    linearLayout.addView(newLL);

                    newLL = new LinearLayout(context);
                    newLL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    newLL.setOrientation(LinearLayout.HORIZONTAL);
                    newLL.setGravity(Gravity.CENTER);
                    params = new LinearLayout.LayoutParams(LL.getMeasuredWidth(), LL.getMeasuredHeight());
                    newLL.addView(LL, params);
                    widthSoFar = LL.getMeasuredWidth();
                } else {
                    newLL.addView(LL);
                }
            }
            linearLayout.addView(newLL);
        }
    }

    public static class TabsAdapter extends FragmentStatePagerAdapter implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
        private final Context mContext;
        private final TabHost mTabHost;
        public final ViewPager mViewPager;
        private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

        static final class TabInfo {
            private final String tag;
            private final Class<?> clss;
            private final Bundle args;

            TabInfo(String _tag, Class<?> _class, Bundle _args) {
                tag = _tag;
                clss = _class;
                args = _args;
            }
        }

        static class DummyTabFactory implements TabHost.TabContentFactory {
            private final Context mContext;

            public DummyTabFactory(Context context) {
                mContext = context;
            }

            public View createTabContent(String tag) {
                View v = new View(mContext);
                v.setMinimumWidth(0);
                v.setMinimumHeight(0);
                return v;
            }
        }

        public TabsAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
            super(activity.getSupportFragmentManager());
            mContext = activity;
            mTabHost = tabHost;
            mViewPager = pager;
            mTabHost.setOnTabChangedListener(this);
            mViewPager.setAdapter(this);
            mViewPager.setOnPageChangeListener(this);
        }

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
            tabSpec.setContent(new DummyTabFactory(mContext));
            String tag = tabSpec.getTag();

            TabInfo info = new TabInfo(tag, clss, args);
            mTabs.add(info);
            mTabHost.addTab(tabSpec);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public Fragment getItem(int position) {
            TabInfo info = mTabs.get(position);
            Fragment fraga = Fragment.instantiate(mContext, info.clss.getName(), info.args);
            registeredFragments.put(position, fraga);
            return fraga;

        }

        public void onTabChanged(String tabId) {
            int position = mTabHost.getCurrentTab();
            mViewPager.setCurrentItem(position);


            if (tabId.equals("wants")) {
                IWFragmentWts fa = (IWFragmentWts) getRegisteredFragment(0);
                if (fa != null) {
                    ProgressDialog progressDialog = new ProgressDialog(mContext);
                    progressDialog.setMessage("Синхронизация");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    ((IWWants) mContext).titleView.setText("Желания");
                    fa.sync(progressDialog);
                }
            }
            if (tabId.equals("others")) {
                ((IWWants) mContext).titleView.setText("Прочие");
            }
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public void onPageSelected(int position) {
            TabWidget widget = mTabHost.getTabWidget();
            int oldFocusability = widget.getDescendantFocusability();
            widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            mTabHost.setCurrentTab(position);
            widget.setDescendantFocusability(oldFocusability);
        }

        public void onPageScrollStateChanged(int state) {

        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
