package dev.mabee.iwant;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Ed on 31.10.2014.
 */
public class IWActivateActivity extends Activity {

    Bundle bundle;
    String number;
    EditText textCode;
    BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activatecode_screen);

        bundle = getIntent().getExtras();
        number = bundle.getString("number");

        textCode = (EditText) findViewById(R.id.activate_screen_editboxCode);
        textCode.setOnKeyListener(new EditText.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                boolean flag = false;
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    iwActivateActivity_ClickOk(null);
                    flag = true;
                }
                return flag;
            }
        });
        Button sendAgain = (Button) findViewById(R.id.activate_screen_sendagaing);
        sendAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(IWActivateActivity.this);
                progressDialog.setMessage("Синхронизация...");
                progressDialog.show();
                IWServer.getInstance().sendRequestRCode(number, new IWCallback() {
                    @Override
                    public void call(Response data) {
                        try {
                            Log.i("mLogs","Code : " + data.code() + "  Message : " + data.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                });
            }
        });
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.login"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {}
    }

    public void iwActivateActivty_ClickCancel(View view) {
        onBackPressed();
    }

    public void iwActivateActivity_ClickOk(View view) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Синхронизация...");
        dialog.show();



        final SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        final String number = preferences.getString(IWUserModel.userPhone, "");

        String smsCode = textCode.getText().toString();
        String phone = number.replace("+7", "");

        IWServer.getInstance().sendRequestActivate(smsCode, phone, new IWCallback() {
            @Override
            public void call(Response data) {
                Response resp = data;

                if (resp.code() != 200) {
                    IWActivateActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final AlertDialog alertDialog = new AlertDialog.Builder(IWActivateActivity.this).create();
                            alertDialog.setMessage("Неверный код");
                            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", (DialogInterface.OnClickListener) null);
                            alertDialog.show();
                        }
                    });
                } else {
                    try {
                        final String jsonData = new String(resp.body().bytes());
                        JSONObject objectResp = new JSONObject(jsonData);

                        final String uid = objectResp.getString(IWUserModel.genUID);

                        IWServer.getInstance().sendRequestSessionId(number, uid, new IWCallback() {
                            @Override
                            public void call(Response data) {

                                String sessionID = null;
                                try {
                                    String bd = data.body().string();
                                    JSONObject jsonObject = new JSONObject(bd);
                                    sessionID = jsonObject.getString(IWUserModel.sessionID);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString(IWUserModel.genUID, uid);
                                editor.putString(IWUserModel.sessionID, sessionID);
                                editor.commit();
                            }
                        });


                        Intent intent = new Intent(IWActivateActivity.this, IWHomeActivity.class);
                        intent.putExtra("firstStart", true);
                        startActivityForResult(intent, 25);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


                dialog.dismiss();
            }
        });
    }
}
