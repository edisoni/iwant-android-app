package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.*;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Ed on 31.10.2014.
 */


public class IWRegActivity extends FragmentActivity {


    RadioButton radioButtonFiz;
    EditText textEntity;
    EditText textFamily;
    EditText textName;
    EditText textNumber;
    EditText textOtch;
    TextView textView;
    TextView textRegion;
    TextView textCity;
    String cityId = "";
    BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_screen);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.login"));

        textEntity = (EditText) findViewById(R.id.reg_screen_eb_entity);
        textFamily = (EditText) findViewById(R.id.reg_screen_eb_family);
        textName = (EditText) findViewById(R.id.reg_screen_eb_name);
        textNumber = (EditText) findViewById(R.id.reg_screen_eb_number);
        textOtch = (EditText) findViewById(R.id.reg_screen_eb_otchname);
        textCity = (TextView) findViewById(R.id.reg_screen_tv_town);
        textRegion = (TextView) findViewById(R.id.reg_screen_tv_region);

        final RelativeLayout entLay = (RelativeLayout) findViewById(R.id.reg_screen_rl_entity);
        radioButtonFiz = (RadioButton) findViewById(R.id.reg_screen_rb_fiz_face);
        radioButtonFiz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    entLay.setVisibility(View.GONE);
                } else {
                    entLay.setVisibility(View.VISIBLE);
                }
            }
        });

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.reg_screen_ln_town);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IWRegActivity.this, IWSelectTown.class);
                startActivityForResult(intent, 5);
            }
        });

        TextView textLicense = (TextView) findViewById(R.id.textLicense);
        textLicense.setText(Html.fromHtml("<p> Нажимая кнопку готово, Вы соглашаетесь с условием <span style=\"color:#008080;\"><u>пользовательского соглашения</u>.</span></p>"));
        textLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(IWRegActivity.this);
                alert.setTitle("Пользовательское соглашение");
                WebView wv = new WebView(IWRegActivity.this);
                wv.loadData(license, "text/html; charset=UTF-8", null);
                wv.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }
                });
                alert.setView(wv);
                alert.show();
            }
        });



        textView= (TextView) findViewById(R.id.reg_screen_tv_fromknow);
        LinearLayout llFromKnow = (LinearLayout) findViewById(R.id.reg_screen_ln_fromknow);
        llFromKnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(IWRegActivity.this);
                progressDialog.setMessage("Получение данных");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(IWRegActivity.this, android.R.layout.select_dialog_singlechoice);
                IWServer.getInstance().sendRequestFromKnow(new IWCallback() {
                    @Override
                    public void call(Response data) {
                        progressDialog.dismiss();
                        try {
                            JSONArray array = new JSONArray(data.body().string());
                            for (int i = 0; i < array.length(); i++) {
                                String element = array.getString(i);
                                arrayAdapter.add(element);
                            }
                            IWRegActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(IWRegActivity.this);
                                    builderSingle.setTitle("Откуда вы узнали?");

                                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String strName = arrayAdapter.getItem(which);
                                            textView.setText(strName);
                                        }
                                    });
                                    builderSingle.show();
                                }
                            });
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });


            }
        });


        textFamily.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String result = editable.toString().replaceAll(" ", "");
                if (!editable.toString().equals(result)) {
                    textFamily.setText(result);
                    textFamily.setSelection(result.length());
                }
            }
        });
        textName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String result = editable.toString().replaceAll(" ", "");
                    if (!editable.toString().equals(result)) {
                        textName.setText(result);
                    textName.setSelection(result.length());
                }
            }
        });
        textOtch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String result = editable.toString().replaceAll(" ", "");
                if (!editable.toString().equals(result)) {
                    textOtch.setText(result);
                    textOtch.setSelection(result.length());
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {

        }
    }

    public void iwRegActivity_ClickOk(View view) {

        if (textFamily.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWRegActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения ФИО");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (textName.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWRegActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения ФИО");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (textOtch.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWRegActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения ФИО");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (!radioButtonFiz.isChecked()) {
            if (textEntity.getText().toString().isEmpty()) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWRegActivity.this);
                alertDialog.setMessage("Проверьте правильность наименования юр.лица");
                alertDialog.setTitle("Ошибка");
                alertDialog.setPositiveButton("ОК", null);
                alertDialog.create().show();
                return;
            }
        }
        if (textNumber.getText().toString().length()<10 || textNumber.getText().toString().isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWRegActivity.this);
            alertDialog.setMessage("Проверьте правильность заполнения номера телефона");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }
        if (cityId.isEmpty()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWRegActivity.this);
            alertDialog.setMessage("Выберите населенный пункт");
            alertDialog.setTitle("Ошибка");
            alertDialog.setPositiveButton("ОК", null);
            alertDialog.create().show();
            return;
        }



        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Синхронизация...");
        dialog.show();

        final String family = textFamily.getText().toString();
        final String name = textName.getText().toString();
        final String number = textNumber.getText().toString();
        final String otch = textOtch.getText().toString();
        final String entity = textEntity.getText().toString();
        final String status = radioButtonFiz.isChecked() ? "1" : "2";
        String advsource = textView.getText().toString();
        final String city    = textCity.getText().toString();
        final String region  = textRegion.getText().toString();



        final JSONObject object = IWUserModel.createJObject(name,
                family,
                otch,
                number,
                cityId,
                status,
                entity,
                advsource);

        IWServer.getInstance().sendRequestRegistraion(object, new IWCallback() {
            @Override
            public void call(Response data) {
                dialog.dismiss();
                if (data.code()==200) {

                    SharedPreferences.Editor preferences = getSharedPreferences("iwant_data", MODE_PRIVATE).edit();
                    preferences.putString(IWUserModel.userCity, city);
                    preferences.putString(IWUserModel.userEntityName, entity);
                    preferences.putString(IWUserModel.userFirstName, name);
                    preferences.putString(IWUserModel.userLastName, family);
                    preferences.putString(IWUserModel.userSecondName, otch);
                    preferences.putString(IWUserModel.userPhone, number);
                    preferences.putString(IWUserModel.userStatus, status);
                    preferences.putString(IWUserModel.userRegion, region);
                    preferences.putString(IWUserModel.userCityID, cityId);
                    preferences.commit();

                    Intent intent = new Intent(IWRegActivity.this, IWActivateActivity.class);
                    intent.putExtra("number",number);
                    startActivityForResult(intent,25);
                } else {
                    IWRegActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWRegActivity.this);
                            alertDialog.setMessage("Войдите в систему");
                            alertDialog.setPositiveButton("ОК", null);
                            alertDialog.show();
                        }
                    });
                }
            }
        });


    }


    public void iwRegActivity_ClickCancel(View view) {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 25 && resultCode == 403) {
            setResult(403);
            finish();
        }
        if (resultCode == 5) {
            String town = data.getExtras().getString("town");
            cityId = data.getExtras().getString("id");
            String region = data.getExtras().getString("region");

            TextView textView = (TextView) findViewById(R.id.reg_screen_tv_town);
            textView.setText(town);
            TextView textViewr = (TextView) findViewById(R.id.reg_screen_tv_region);
            textViewr.setText(region);
        }

    }





    String license = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<body> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
            "\t<h3>Пользовательское соглашение об условиях использования мобильного приложения «Я хочу»</h3>\n" +
            "\n" +
            "\t<h4>1. ОБЩИЕ ПОЛОЖЕНИЯ</h4>\n" +
            "\t<p>1.1. Настоящее Пользовательское соглашение (далее - «Соглашение») регламентирует отношения между ООО «Идея», имеющим адрес местонахождения 355008, Ставрополь г., Вокзальная ул. , дом 12, к2. (далее – «Компания»), и лицом, использующим мобильное приложение «Я хочу» (далее – «Пользователь» и «Приложение» соответственно). Условия настоящего Соглашения являются публичной офертой в соответствии со ст. 437 Гражданского кодекса Российской Фeдерации и использование Приложения возможно исключительно на условиях настоящего Соглашения. </p>\n" +
            "\t<p>1.2. Прохождение процедуры активации Приложения в соответствии с пунктом 2.2 настоящего Соглашения (далее – «Регистрация») и дальнейшее использование Приложения является подтверждением согласия со всеми условиями, указанными в настоящем Соглашении. Компания признает Пользователем любое лицо, которое прошло процедуру Регистрации в Приложении и использует Приложение в соответствии с его функциональным назначением. На таких лиц распространяются положения данного Соглашения в соответствии со статьями 437 и 438 Гражданского кодекса Российской Федерации.</p>\n" +
            "\t<p>1.3. Принимая условия настоящего Соглашения, Пользователь подтверждает свое согласие на обработку Компанией его персональных данных, предоставленных при регистрации, в том числе, но не ограничиваясь, для формирования и передачи ответа Пользователю, а также разрешения возможных претензий. Также Пользователь подтверждает свое согласие с передачей указанных выше персональных данных третьим лицам и их обработку третьими лицами в целях исполнения настоящего Соглашения и реализации функционирования Приложения, а также разрешения претензий, связанных с исполнением настоящего Соглашения.</p>\n" +
            "\t<p>1.4. Настоящее Соглашение может быть изменено и/или дополнено Компанией в одностороннем порядке. При этом продолжение использования Приложения после внесения изменений и/или дополнений в настоящее Соглашение, означает согласие Пользователя с такими изменениями и/или дополнениями. В случае несогласия с условиями такого измененного Соглашения, Пользователь обязан отказаться от дальнейшего использования Приложения.</p>\n" +
            "\t<p>1.5. Обращения, предложения и претензии физических и юридических лиц к Компании, связанные с содержанием и функционированием Приложения, нарушениями прав и интересов третьих лиц, требований законодательства Российской Федерации, а также для запросов уполномоченных законодательством Российской Федерации лиц могут быть направлены на адрес электронной почты: ooo_idea@inbox.ru.</p>\n" +
            "\t<p>1.6. Настоящее Соглашение составлено в соответствии с законодательством Российской Федерации. Вопросы, не урегулированные Соглашением, подлежат разрешению в соответствии с законодательством Российской Федерации.</p>\n" +
            "\t<p>1.7. Соглашаясь с условиями настоящего Соглашения, Пользователь подтверждает свои право и дееспособность, подтверждает достоверность введенных им при регистрации данных и принимает на себя всю ответственность за их точность, полноту и достоверность.</p>\n" +
            "\t<p>1.8. Пользователь принимает на себя все возможные риски, связанные с допущенными им ошибками и неточностями в предоставленных данных.</p>\n" +
            "\n" +
            "\t<h4>2. ФУНКЦИОНАЛЬНЫЕ ВОЗМОЖНОСТИ ПРИЛОЖЕНИЯ</h4>\n" +
            "    <p>2.1. Мобильное приложение «Я хочу» является программой для ЭВМ, представляющей собой информационное приложение, разработанное для мобильных устройств, работающих под управлением операционных систем Apple iOS и Android.</p>\n" +
            "    <p>2.2. До начала использования Приложения, Пользователь осуществляет его активацию, путем прохождения процедуры Регистрации: указания в Приложении номера своего телефона, Фамилии, Имени, Отчества и места работы.</p>\n" +
            "    <p>2.3. В случае изменения данных, указанных при Регистрации, Пользователь обязуется незамедлительно внести изменения в Регистрационные данные через Приложение.</p>\n" +
            "    <p>2.4. За использование Приложением с Пользователя может взиматься плата, размер которой доводится до сведения Пользователя через Приложение. Вознаграждение списывается со счета телефона или со счета/с банковской карты Пользователя, с которой осуществляется перевод денежных средств.</p>\n" +
            "    \n" +
            "    <h4>3. ПРАВА И ОБЯЗАННОСТИ ПОЛЬЗОВАТЕЛЯ</h4>\n" +
            "    <p>3.1. Пользователь обязуется надлежащим образом соблюдать условия настоящего Соглашения.</p>\n" +
            "    <p>3.2. Пользователь обязуется не использовать Приложение в нарушение прав и законных интересов правообладателей, третьих лиц, настоящего Соглашения и законодательства РФ.</p>\n" +
            "    <p>3.3. Пользователь обязуется принимать надлежащие меры для обеспечения сохранности его мобильного устройства и несет личную ответственность в случае доступа к его мобильному устройству третьих лиц. При утрате телефона, Пользователь обязан незамедлительно сообщить всем поставщикам услуг, эмитировавшими карты для Пользователя или открывшие счет, о необходимости блокирования карт и счетов.</p>\n" +
            "    <p>3.4. Пользователю запрещено самостоятельно или с привлечением третьих лиц осуществлять декомпилирование Приложения, а также распространять, доводить до всеобщего сведения и предоставлять иной доступ к Приложению, осуществлять реверс-инжиниринг Приложения или его отдельных элементов.</p>\n" +
            "    <p>3.5. Пользователь несет ответственность за использование Приложения и его сервисов любыми способами, прямо не разрешенными в настоящем Соглашении.</p>\n" +
            "    <p>3.6. Любые платежи за услуги связи, в том числе оказываемые операторами сотовой связи или поставщиками интернет услуг, уплачиваются Пользователем самостоятельно.</p>\n" +
            "    <p>3.7. При регистрации в Приложении Пользователь обязуется сообщать достоверную и актуальную информацию о себе.</p>\n" +
            "    <p>3.8. Пользователь обязуется, пользуясь Приложением, не вводить в заблуждение других Пользователей и третьих лиц.</p>\n" +
            "    <p>3.9. Пользователь обязуется посредством Приложения не проводить рассылок сообщений рекламного типа без согласия компании. Такое согласие должно быть получено в случаях и в форме, предусмотренных законодательством Российской Федерации.</p>\n" +
            "    <p>3.10. Пользователь обязуется не использовать Приложение в нарушение прав и законных интересов третьих лиц, абонентов операторов сети связи и законодательства Российской Федерации, в том числе</p>\n" +
            "    <p>3.11. Пользователь обязуется не размещать посредством Приложения материалы, содержащие оскорбления, клевету, нецензурные выражения, порнографические или иные противоречащие нормам морали материалы; материалы, демонстрирующие или пропагандирующие жестокость, террор или насилие, оскорбляющие человеческое достоинство, а также иные материалы, не соответствующие законодательству Российской Федерации или ссылки на интернет-сайты и другие ресурсы, содержащие такие материалы.</p>\n" +
            "    <p>3.12. Пользователь обязуется не размещать любую информацию и материалы, которые содержат угрозы, дискредитируют или оскорбляют других Пользователей или третьих лиц, носят мошеннический характер, посягают на личные или публичные интересы, пропагандируют расовую, религиозную, этническую ненависть или вражду, а также любую иную информацию, нарушающую охраняемые законодательством Российской Федерации права человека и гражданина.</p>\n" +
            "    <p>3.13. Пользователь обязуется не осуществлять пропаганду или агитацию, возбуждающую социальную, расовую, национальную или религиозную ненависть и вражду, ненависть к лицам нестандартной сексуальной ориентации, пропаганду войны, социального, расового, национального, религиозного или языкового превосходства.</p>\n" +
            "    <p>3.14. Пользователь обязуется не описывать или пропагандировать преступную деятельность, размещать инструкции или руководства по совершению преступных действий.</p>\n" +
            "    <p>3.15. Пользователь обязуется не размещать или передавать посредством Приложения любую информацию ограниченного доступа (конфиденциальную информацию), если Пользователь не уполномочен на совершение данных действий.</p>\n" +
            "    <p>3.16. Пользователь обязуется не распространять посредством Приложения спам, сообщения-цепочки (сообщения, требующие их передачи одному или нескольким пользователям), схемы финансовых пирамид или призывы в них участвовать, а также любую другую навязчивую информацию.</p>\n" +
            "    <p>3.17. Пользователь обязуется не размещать посредством Приложения сообщения, графические изображения, фотографии или другие материалы, размещение которых наносит или может нанести ущерб чести, достоинству и деловой репутации какого-либо физического или юридического лица.</p>\n" +
            "    <p>3.18. Пользователь обязуется не размещать посредством Приложения персональные данные, в том числе домашние адреса, номера телефонов, адреса электронной почты, паспортные данные и прочую личную информацию других Пользователей или иных лиц без их предварительного согласия.</p>\n" +
            "    <p>3.19. Пользователь обязуется не размещать посредством Приложения результаты интеллектуальной деятельности, к которым в том числе относятся музыкальные произведения с текстом или без текста, литературные произведения, произведения живописи, графики, дизайна, аудиовизуальные произведения, программы для ЭВМ, фотографические произведения, фонограммы, исполнения, записанные в фонограммах, иные подобные объекты авторского и смежных прав, которые могут быть представлены в интерактивном виде (далее – «Контент»), права на использование которых подобным способом отсутствуют у Пользователя.</p>\n" +
            "    <p>3.20. Пользователь обязуется не размещать в Приложении Контент, содержащий изображения физических лиц, без их предварительного согласия за исключением случаев, предусмотренных законодательством Российской Федерации.</p>\n" +
            "    \n" +
            "    <h4>4. ПРАВА И ОБЯЗАННОСТИ КОМПАНИИ</h4>\n" +
            "    <p>4.1. Компания вправе передавать права и обязанности по настоящему Соглашению третьим лицам в целях исполнения настоящего Соглашения, без дополнительного согласия Пользователя.</p>\n" +
            "    <p>4.2. Пользователь, соглашаясь с данной офертой, дает свое информированное и добровольное согласие на участие в стимулирующих, рекламных, маркетинговых и иных мероприятиях, направленных на продвижение услуг Компании, партнеров Компании и иных третьих лиц. Компания вправе направлять Пользователю любым способом информацию о функционировании Приложения, в том числе на номер телефона, указанный Пользователем, а также направлять собственные или любых третьих лиц информационные, рекламные или иные сообщения, или размещать соответствующую информацию в самом Приложении.</p>\n" +
            "    <p>4.3. Компания вправе оказывать Пользователям платные и бесплатные услуги. Об условиях предоставления платных услуг Компания информирует Пользователя путем размещения в Приложении или на интернет-сайте соответствующей информации об услуге (наименование услуги, ее стоимость, форма и порядок оплаты).</p>\n" +
            "    <p>4.4. Компания вправе заблокировать доступ Пользователя к Приложению, удалять опубликованную информацию пользователя в Приложении в случае обнаружения нарушений Пользователем обязанностей, указанных в разделе 3 настоящего Соглашения.</p>\n" +
            "    <p>4.5. Пользователь дает свое согласие Компании в целях исполнения настоящего Соглашения, направления Пользователю любого рода сообщений и разрешения претензий, связанных с исполнением настоящего Соглашения осуществлять обработку персональных данных Пользователя, передавать их третьим лицам, а равно привлекать третьих лиц к обработке персональных данных Пользователя без выплаты Пользователю вознаграждения, как без использования средств автоматизации, так и с их использованием. В ходе обработки персональных данных могут быть совершены следующие действия: сбор, запись, систематизация, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передача (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение, а также любые иные действия. Пользователь соглашается, что персональные данные могут обрабатываться в течение срока деятельности Исполнителя. Хранение персональных данных осуществляется согласно действующему законодательству РФ.</p>\n" +
            "    <p>4.6. Компания оставляет за собой право в любой момент расторгнуть настоящее Соглашение по организационным или техническим причинам в одностороннем порядке, удалив Приложение с мобильного устройства Пользователя или заблокировав возможность его использования.</p>\n" +
            "    <p>4.7. В целях улучшения и повышения стабильности работы Приложения Компания вправе собирать, хранить и обрабатывать статистическую информацию об использовании Пользователем Приложения.</p>\n" +
            "    \n" +
            "    <h4>5. ГАРАНТИИ И ОТВЕТСТВЕННОСТЬ СТОРОН</h4>\n" +
            "    <p>5.1. Для исполнения настоящего Соглашения могут привлекаться третьи лица. Пользователь подтверждает, что указанным третьим лицам предоставляются такие же права, как и обладателю прав на Приложение, в том числе в отношении персональных данных Пользователя.</p>\n" +
            "    <p>5.2. Пользователь гарантирует, что не будет предпринимать каких-либо действий, направленных исключительно на причинение ущерба Компании, операторам сотовой мобильной связи, правообладателям или иным лицам.</p>\n" +
            "    <p>5.3. В случае нарушения правил использования Приложения, указанных в разделе 3 настоящего Соглашения, а также в случае нарушения пункта 5.2 настоящего Соглашения, Пользователь обязуется возместить Компании вред, причиненный такими действиями.</p>\n" +
            "    <p>5.4. Если Пользователем не доказано обратное, любые действия, совершенные с использованием его мобильного устройства, считаются совершенными соответствующим Пользователем.</p>\n" +
            "    <p>5.5. Компания не гарантирует, что Приложение и его отдельные элементы не содержат ошибок и будут функционировать в соответствии с ожиданиями Пользователя. Наличие ошибок или недостатков в Приложении, которое в том числе ведет к невозможности функционирования Приложения на мобильном устройстве Пользователя, не является основанием для обмена, возврата или ремонта такого мобильного устройства. Также Компания не предоставляет гарантий относительно информации, предоставляемой через Приложение. Любая предоставляемая в Приложении информация является результатом обработки запроса и поиска запрошенной информации, при этом Компания не является лицом, размещающим такую информацию, публикующую или предоставляющую ее Пользователю или иным третьим лицам. Любая такая информация предоставляется непосредственно из сети Интернет и Компания не может гарантировать, что такая информация будет соответствовать подобающей возрастной категории. В связи с этим Компания предупреждает о том, что предоставляемый из сети Интернет информационный контент может быть не предназначен для лиц младше 18 лет</p>\n" +
            "    \n" +
            "    <h4>6. ССЫЛКИ НА САЙТЫ ТРЕТЬИХ ЛИЦ</h4>\n" +
            "    <p>6.1. Приложение может содержать ссылки или представлять доступ на другие сайты в сети Интернет (сайты третьих лиц) и размещенный на данных сайтах контент, являющиеся результатом интеллектуальной деятельности третьих лиц и охраняемые в соответствии с законодательством Российской Федерации. Указанные сайты и размещенный на них контент не проверяются Компанией на соответствие требованиям законодательства Российской Федерации.</p>\n" +
            "    <p>6.2. Компания не несет ответственность за любую информацию или контент, размещенные на сайтах третьих лиц, к которым Пользователь получает доступ посредством Приложения, включая, в том числе, любые мнения или утверждения, выраженные на сайтах третьих лиц.</p>\n" +
            "    <p>6.3. Пользователь подтверждает, что с момента перехода Пользователя по ссылке, содержащейся в Приложении, на сайт третьего лица, взаимоотношения компании и Пользователя прекращаются, настоящее Соглашение в дальнейшем не распространяется на Пользователя, и Компания не несет ответственность за использование Пользователем контента, правомерность такого использования и качество контента, размещенного на сайтах третьих лиц.</p>\n" +
            "    \n" +
            "    <h4>7. ЗАКЛЮЧИТЕЛЬНЫЕ ПОЛОЖЕНИЯ</h4>\n" +
            "    <p>7.1. В случае возникновения любых споров или разногласий, связанных с исполнением настоящего Соглашения, Пользователь и Компания приложат все усилия для их разрешения путем проведения переговоров между ними. В случае, если споры не будут разрешены путем переговоров, споры подлежат разрешению в соответствующем компетентном суде по месту нахождения Компании в порядке, установленном действующим законодательством Российской Федерации.</p>\n" +
            "    <p>7.2. Настоящее Соглашение вступает в силу для Пользователя с момента Регистрации в соответствии с пунктом 2.2 Соглашения и действует до тех пор, пока не будет изменено или расторгнуто по инициативе Компании.</p>\n" +
            "    <p>7.3. Настоящее Соглашение составлено на русском языке.</p>\n" +
            "    <p>7.4. Если какое-либо из положений настоящего Соглашения будет признано недействительным, это не оказывает влияния на действительность или применимость остальных положений настоящего Соглашения.</p>\n" +
            "</body>\n" +
            "</html>";
}
