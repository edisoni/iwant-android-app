package dev.mabee.iwant;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.*;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONObject;
import org.onepf.oms.OpenIabHelper;
import org.onepf.oms.appstore.googleUtils.IabHelper;
import org.onepf.oms.appstore.googleUtils.IabResult;
import org.onepf.oms.appstore.googleUtils.Purchase;

import java.io.IOException;

/**
 * Created by Ed on 07.11.2014.
 */
public class IWHomeActivity extends Activity {

    boolean firstClient = false;
    boolean firstExecutor = false;
    BroadcastReceiver broadcastReceiver;

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String SENDER_ID = "194035531498";
    String regid;
    GoogleCloudMessaging gcm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);
        setResult(403);
        final TextView textView = (TextView) findViewById(R.id.home_screen_text_successwants);




        // Закрываем предыдущие  активити авторизации/регистрации
        Intent killOther = new Intent("dev.mabee.login");
        sendBroadcast(killOther);


        broadcastReceiver =new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.relog"));


        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) {
            if (bundle.getString("FLAG") != null) {
                Intent intent = new Intent(this, IWActionsActivity.class);
                intent.putExtra("FLAG", "YES");
                startActivity(intent);
            }
        }
        IWServer.getInstance().sendRequestWantCount(new IWCallback() {
            @Override
            public void call(Response data) {
                try {
                    Activity act = IWHomeActivity.this;
                    if (act != null) {
                        JSONObject obj = new JSONObject(data.body().string());
                        final String wants_count = obj.getString("wants_count");
                        IWHomeActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textView.setText(Html.fromHtml("<font color=#9A9797>Выполненных желаний</font><br><center><font color=#0286c6>" + wants_count + "</font></center>"));
                            }
                        });

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        if (!isOnline()) {
            Intent intent = new Intent(getApplicationContext(), IWConnectionLost.class);
            startActivity(intent);
            return;
        }

        ImageView editBtn = (ImageView) findViewById(R.id.home_screen_editbutton);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), IWEditActivity.class);
                startActivity(intent);
            }
        });




    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public void iwHomeActivity_ClickGoods(View view) {
        SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        firstClient = preferences.getBoolean("firstClient", true);
        firstExecutor = preferences.getBoolean("firstExecutor", true);

        if (firstClient) {
            Intent intent = new Intent(this, IWGuideClientActivity.class);
            startActivity(intent);
            SharedPreferences.Editor editor = getSharedPreferences("iwant_data", MODE_PRIVATE).edit();
            editor.putBoolean("firstClient", false);
            editor.apply();
        } else {
            Intent intent = new Intent(this, IWWants.class);
            startActivity(intent);
        }
    }

    public void iwHomeActivity_ClickMakingAct(View view) {
        SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        firstClient = preferences.getBoolean("firstClient", true);
        firstExecutor = preferences.getBoolean("firstExecutor", true);

        if (firstExecutor) {
            Intent intent = new Intent(this, IWGuideExecutorActivity.class);
            startActivity(intent);
            SharedPreferences.Editor editor = getSharedPreferences("iwant_data", MODE_PRIVATE).edit();
            editor.putBoolean("firstExecutor", false);
            editor.apply();
        } else {
            Intent intent = new Intent(this, IWActionsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Context context = getApplicationContext();
        Intent i = new Intent(context,IWStartActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("FLAG","YES");


        SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        String name = preferences.getString(IWUserModel.userFirstName, "noneName");
        String otch = preferences.getString(IWUserModel.userSecondName, "noneLastName");

        TextView textWelcome = (TextView) findViewById(R.id.reg_screen_tv_welcome);
        textWelcome.setText("Здравствуйте,\n" + name + " " + otch);

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);

            regid = getRegistrationId(this);

            Log.i("mLogs","Application on Start RegID : " + regid);
            if (regid.isEmpty()) {
                registerInBackground();
            } else {
                IWServer.getInstance().sendRequestToken(preferences.getString(IWUserModel.sessionID, ""), regid, null);
            }
            
        } else {
            Log.i("mLogs", "No valid Google Play Services APK found.");
        }
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(IWHomeActivity.this);
                    }
                    regid = gcm.register(SENDER_ID);
                    Log.i("mLogs","Application on Background RegID : " + regid);

                    // Send on server token
                    SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
                    IWServer.getInstance().sendRequestToken(preferences.getString(IWUserModel.sessionID, ""), regid, null);

                    storeRegistrationId(IWHomeActivity.this, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }


    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getGcmPreferences(Context context) {
        return getSharedPreferences("iwant_data", Context.MODE_PRIVATE);
    }
}
