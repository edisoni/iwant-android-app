package dev.mabee.iwant;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;
import org.w3c.dom.Text;

/**
 * Created by Ed on 03.12.2014.
 */


public class IWInfoOrder extends FragmentActivity {
    BroadcastReceiver broadcastReceiver;

    String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infoorder_screen);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String text  = bundle.getString("text");
            phone = bundle.getString("phone");
            String fio   = bundle.getString("fio");
            String call_time_start = bundle.getString("call_time_start");
            String call_time_end = bundle.getString("call_time_end");
            String region= bundle.getString("region");
            String city  = bundle.getString("city");
            String entity = bundle.getString("entity");

            TextView textEntity = (TextView) findViewById(R.id.infoorder_screen_entity);
            TextView textText = (TextView) findViewById(R.id.infoorder_screen_text);
            TextView textPhone = (TextView) findViewById(R.id.infoorder_screen_phone);
            TextView textTimed = (TextView) findViewById(R.id.infoorder_screen_timed);
            TextView textFio = (TextView) findViewById(R.id.infoorder_screen_fio);
            TextView textRegion = (TextView) findViewById(R.id.infoorder_screen_region);
            TextView textCity = (TextView) findViewById(R.id.infoorder_screen_city);


            textPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    String str = "tel:"+"+7"+phone;
                    intent.setData(Uri.parse(str));
                    startActivity(intent);
                }
            });

            if (entity==null) {
               textEntity.setVisibility(View.GONE);
            } else {
               textEntity.setText(entity);
            }

            textText.setText(text);
            textPhone.setText("+7" + phone);
            textFio.setText(fio);
            textTimed.setText("C "+call_time_start + " До " + call_time_end);
            textRegion.setText(region);
            textCity.setText(city);
        }
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.relog"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {

        }
    }

    public void iwInfoOrder_ClickBack(View view) {
        onBackPressed();
    }

    public void iwInfoOrder_ClickCall(View view) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        String str = "tel:+7"+phone;
        intent.setData(Uri.parse(str));
        startActivity(intent);
    }
}
