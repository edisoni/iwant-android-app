package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.*;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.*;
import android.text.style.DynamicDrawableSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import android.widget.*;
import com.kemallette.RichEditText.Widget.RichEditText;
import com.kemallette.RichEditText.Widget.RichEditTextField;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

/**
 * Created by Ed on 12.11.2014.
 */
public class IWAddWant extends FragmentActivity {
    Timer[] timers;

    String time1 = "10:00";
    String time2 = "19:00";
    String countExecutors;
    CheckBox boxFiz;
    CheckBox boxUr;
    EditText editText;

    BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addwant_screen);


        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.relog"));

        timers = new Timer[1];


        FrameLayout frameLayoutw = (FrameLayout) findViewById(R.id.addwant_screen_select_timew);
        FrameLayout frameLayouto = (FrameLayout) findViewById(R.id.addwant_screen_select_timeo);

        final TextView textView = (TextView) findViewById(R.id.addwant_screen_executorcount_tv);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.addwant_screen_executorcount);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IWAddWant.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final Dialog dialog = new Dialog(IWAddWant.this);
                        dialog.setContentView(R.layout.addwant_executors_dialog);
                        dialog.setTitle("Количество конкурентов");
                        ListView listView = (ListView) dialog.findViewById(R.id.addwant_screen_listview);

                        ArrayList<String> list = new ArrayList<String>();
                        list.add("1");
                        list.add("2");
                        list.add("3");
                        list.add("4");
                        list.add("5");

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(IWAddWant.this, R.layout.addwant_screen_row, R.id.addwant_screen_row_tv, list);
                        listView.setAdapter(adapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                textView.setText("" + (1 + i));
                                countExecutors = "" + (i + 1);
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                });

            }
        });

        boxFiz = (CheckBox) findViewById(R.id.addwant_screen_fizlico);
        boxUr = (CheckBox) findViewById(R.id.addwant_screen_urlico);

        boxFiz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b && !boxUr.isChecked()) {
                    boxUr.setChecked(true);
                }
            }
        });

        boxUr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b && !boxFiz.isChecked()) {
                    boxFiz.setChecked(true);
                }
            }
        });

        editText = (EditText) findViewById(R.id.addwant_screen_editbox);
        editText.setSelection(7);
        final TextView textCounter = (TextView) findViewById(R.id.addwant_screen_textcounter);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int co = editText.length();
                co -= 7;
                if (co < 0) co = 0;
                textCounter.setText(co + "/150");
            }
            boolean ntimer = false;
            @Override
            public void afterTextChanged(final Editable editable) {
                String currentText = editText.getText().toString();
                int len = currentText.length();
                Log.i("mLogs", "Current Text : " + currentText);
                if (len < 7) {
                    editText.setText("Я хочу ");
                    editText.setSelection(7);
                } else {
                    if (!currentText.substring(0, 7).equals("Я хочу ")) {
                        editText.setText("Я хочу " + editText.getText().toString().substring(8));
                        editText.setSelection(editText.getText().toString().length());
                    }
                }

                if (ntimer) {
                    ntimer = false;
                    return;
                }
                if (timers[0] != null) {
                    timers[0].cancel();
                }

                timers[0] = new Timer();
                timers[0].schedule(new TimerTask() {

                    @Override
                    public void run() {


                        final int beforRequest = editText.getText().toString().length();
                        final SpannableStringBuilder stringBuilder = new SpannableStringBuilder(editText.getText().toString());
                        IWServer.getInstance().sendRequestValidate(editText.getText().toString(), new IWCallback() {
                            @Override
                            public void call(Response data) {
                                String body = null;
                                final int currentPos = editText.getSelectionStart();
                                try {

                                    int afterRequest = editText.getText().toString().length();
                                    if (afterRequest!=beforRequest) {
                                        ntimer = true;
                                        return;
                                    }

                                    body = data.body().string();
                                    JSONArray jsonArray = new JSONArray(body);

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        int pos = jsonObject.getInt("pos");
                                        int len = jsonObject.getInt("len");
                                        for (int j = pos; j < pos + len; j++) {
                                            SpannableString sp = new SpannableString(stringBuilder.subSequence(j, j + 1));
                                            sp.setSpan(new ErrorSpan(getResources()), 0, sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                            stringBuilder.replace(j, j + 1, sp);
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                IWAddWant.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ntimer = true;
                                        Log.i("mLogs", " Update text");
                                        editText.setText(stringBuilder);
                                        editText.setSelection(currentPos);
                                    }
                                });
                            }
                        });
                    }

                }, 2000);

            }
        });


        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == 67 && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String text = editText.getText().toString();
                    if (text.length() < 7) {
                        editText.setText("Я хочу ");
                        editText.setSelection(7);
                    }
                }
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (editText.getText().length() > 9) {
                        InputMethodManager inputManager = (InputMethodManager) IWAddWant.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(IWAddWant.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWAddWant.this);
                        alertDialog.setMessage("Желание должно быть не меньше 3-х символов");
                        alertDialog.create().show();
                    }
                    return true;
                }
                return false;
            }

        });


        final TextView timew = (TextView) findViewById(R.id.addwant_screen_timew);
        final TextView timeo = (TextView) findViewById(R.id.addwant_screen_timeo);

        frameLayoutw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(IWAddWant.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String min = "" + minute;
                        String hour = "" + hourOfDay;
                        if (minute < 9) {
                            min = "0" + min;
                        }
                        if (hourOfDay < 9) {
                            hour = "0" + hour;
                        }
                        time1 = hour + ":" + min;
                        timew.setText(hour + ":" + min);
                    }
                }, 10, 00, true);

                timePickerDialog.show();
            }
        });
        frameLayouto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(IWAddWant.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String min = "" + minute;
                        String hour = "" + hourOfDay;
                        if (minute < 9) {
                            min = "0" + min;
                        }
                        if (hourOfDay < 9) {
                            hour = "0" + hour;
                        }
                        time2 = hour + ":" + min;
                        timeo.setText(hour + ":" + min);
                    }
                }, 19, 00, true);

                timePickerDialog.show();
            }
        });


    }
    public void iwAddWantActivity_helpClick(View view) {

        Intent intent = new Intent(this, IWAddWantDialog.class);
        startActivity(intent);
    }

    public void iwAddWantActivity_readyClick(View view) {
        if (editText.getText().length() > 9) {
            sync();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWAddWant.this);
            alertDialog.setMessage("Желание должно быть не меньше 3-х символов");
            alertDialog.create().show();
        }
    }

    public boolean sync = false;


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {

        }
    }

    public void sync() {
        if (sync) return;
        sync = true;


        try {

            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage("Синхронизация");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();


            String message = editText.getText().toString();


            String status = "0";
            if (boxFiz.isChecked() && boxUr.isChecked()) {
                status = "3";
            } else {
                if (boxFiz.isChecked()) {
                    status = "1";
                } else {
                    status = "2";
                }
            }

            SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
            String uid = UUID.randomUUID().toString();
            String sid = preferences.getString(IWUserModel.sessionID, "none");

            JSONObject want = new JSONObject();
            want.put("uuid", uid);
            want.put("text", message);
            want.put("callTimeStart", time1);
            want.put("callTimeEnd", time2);
            want.put("executorStatus", status);
            want.put("executorLimit", "5");

            JSONArray array = new JSONArray();
            array.put(want);

            JSONObject sendData = new JSONObject();
            sendData.put("session", sid);
            sendData.put("wishes", array);


            IWServer.getInstance().sendRequestAddWant(sendData, new IWCallback() {
                @Override
                public void call(final Response data) {

                    if (data.code() == 508) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                AlertDialog.Builder builder = new AlertDialog.Builder(IWAddWant.this);
                                builder.setTitle("Ошибка");
                                builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                builder.setCancelable(false);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent("dev.mabee.relog");
                                        sendBroadcast(intent);
                                    }
                                });
                                builder.create().show();
                            }
                        });
                        return;
                    }

                    dialog.dismiss();
                    sync = false;
                    if (data.code() == 200) {
                        IWAddWant.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setResult(5);

                                dialog.dismiss();
                                IWAddWant.this.onBackPressed();
                            }
                        });
                    } else {
                        if (data.code() == 202) {
                            String message = "Вы ввели в желание запрещенные слова : ";
                            try {
                                String body = data.body().string();
                                JSONArray jsonArray = new JSONArray(body);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String word = jsonArray.getString(i);
                                    message += word;
                                    if (i + 1 != jsonArray.length()) {
                                        message += ",";
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            final String finalMessage = message;
                            IWAddWant.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(IWAddWant.this);
                                    alertDialog.setPositiveButton("OK", null);
                                    alertDialog.setMessage(finalMessage);
                                    alertDialog.create().show();
                                }
                            });
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void iwAddWantActivity_backClick(View view) {
        onBackPressed();
    }

    static class ErrorSpan extends DynamicDrawableSpan {

        private BitmapDrawable mRedWavy;
        private int mWidth;
        private int mBmpHeight;

        ErrorSpan(Resources resources) {
            super(DynamicDrawableSpan.ALIGN_BASELINE);
            mRedWavy = new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, R.drawable.error_underline));
            mBmpHeight = mRedWavy.getIntrinsicHeight();
            mRedWavy.setTileModeX(Shader.TileMode.REPEAT);
        }

        @Override
        public Drawable getDrawable() {
            return mRedWavy;
        }

        @Override
        public int getSize(Paint paint, CharSequence text,
                           int start, int end,
                           Paint.FontMetricsInt fm) {
            mWidth = (int) paint.measureText(text, start, end);
            return mWidth;
        }


        @Override
        public void draw(Canvas canvas, CharSequence text,
                         int start, int end, float x,
                         int top, int y, int bottom, Paint paint) {

            mRedWavy.setBounds(0, 0, mWidth, mBmpHeight);
            canvas.save();
            canvas.translate(x, bottom - mBmpHeight);
            mRedWavy.draw(canvas);
            canvas.restore();
            canvas.drawText(text.subSequence(start, end).toString(), x, y, paint);
        }
    }

    public static class IWAddWantDialog extends FragmentActivity {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.addwant_screen_dialog);
            TextView textView = (TextView) findViewById(R.id.addwant_screen_closedialog);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });


            TextView text1 = (TextView) findViewById(R.id.addwant_screen_dialog_1);
            TextView text2 = (TextView) findViewById(R.id.addwant_screen_dialog_2);
            TextView text3 = (TextView) findViewById(R.id.addwant_screen_dialog_3);
            TextView text4 = (TextView) findViewById(R.id.addwant_screen_dialog_4);
            TextView text5 = (TextView) findViewById(R.id.addwant_screen_dialog_5);
            TextView text6 = (TextView) findViewById(R.id.addwant_screen_dialog_6);

            Random random = new Random();
            int rnd = random.nextInt(array1.length);
            text1.setText(array1[rnd]);
            rnd = random.nextInt(array2.length);
            text2.setText(array2[rnd]);
            rnd = random.nextInt(array3.length);
            text3.setText(array3[rnd]);
            rnd = random.nextInt(array4.length);
            text4.setText(array4[rnd]);
            rnd = random.nextInt(array5.length);
            text5.setText(array5[rnd]);
            rnd = random.nextInt(array6.length);
            text6.setText(array6[rnd]);
        }

        String[] array1 = new String[]{
                "Я хочу сделать вентиляцию у себя в кафе",
                "Я хочу поставить пять пластиковых окон",
                "Я хочу установить железную входную дверь",
                "Я хочу натяжной потолок в гостинную",
                "Я хочу залить фундамент на участке",
                "Я хочу установить кондиционер",
                "Я хочу поклеить обои в квартире",
                "Я хочу поменять отопление в доме"
        };
        String[] array2 = new String[]{
                "Я хочу поехать на море",
                "Я хочу заниматься тенисом",
                "Я хочу сделать свидание любимой",
                "Я хочу заниматься фитнесом с тренером",
                "Я хочу поехать в Испанию",
                "Я хочу поехать с семьей на рыбалку",
                "Я хочу заниматься спортивными танцами"
        };

        String[] array3 = new String[]{
                "Я хочу сделать свадебную прическу",
                "Я хочу сделать пиллинг",
                "Я хочу купить профессиональную косметику",
                "Я хочу нарастить волосы",
                "Я хочу отбелить зубы",
                "Я хочу сделать татуировку",
                "я хочу сделать маникюр"
        };

        String[] array4 = new String[]{
                "Я хочу обучить коллектив английскому языку",
                "Я хочу отдать ребенка в частный садик",
                "Я хочу учить китайский язык",
                "Я хочу научиться водить авто",
                "Я хочу научиться играть на фортепиано",
                "Я хочу пройти кулинарные курсы"
        };

        String[] array5 = new String[]{
                "Я хочу купить детское автокресло",
                "Я хочу купить бензогенератор",
                "Я хочу поменять газовый счетчик",
                "Я хочу поменять кран на кухне",
                "Я хочу починить ПК",
                "Я хочу провести интернет в квартиру",
                "Я хочу отремонтировать стиральную машину"
        };
        String[] array6 = new String[]{
                "Я хочу построить дом.\nИванов Иван Петрович моб. 89560001111",
                "Я хочу купить велосипед.\nАнастасия. Звоните 89560001111",
                "Я хочу покрасить забор.\nИван. тел. 89560001111. Звоните с 8 до 21."
        };
    }

}
