package dev.mabee.iwant;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWFixedSpeedScroller;
import dev.mabee.iwant.utils.IWUserModel;
import dev.mabee.iwant.utils.IWViewPager;
import org.json.JSONException;
import org.json.JSONObject;
import org.onepf.oms.OpenIabHelper;
import org.onepf.oms.appstore.OpenAppstore;
import org.onepf.oms.appstore.googleUtils.*;
import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by Edisoni on 14.11.2014.
 */


public class IWPushNotifications extends FragmentActivity {

    IWViewPager pager;
    IWAdapter adapter;

    String session;
    String uuid;

    boolean scrolled = true;

    static String statusNotifications = "";
    static String sku = "";
    static String price = "";
    static String days = "30";

    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_screen);
        pager = (IWViewPager) findViewById(R.id.pay_screen_viewpager);
        adapter = new IWAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uuid = bundle.getString("uuid");
            SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
            session = preferences.getString(IWUserModel.sessionID, "none");
        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синхронизация...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        final String session = preferences.getString(IWUserModel.sessionID, "none");
        IWServer.getInstance().sendRequestGetExpired(session, new IWCallback() {
            @Override
            public void call(Response data) {

                try {
                    JSONObject jsonObject = new JSONObject(data.body().string());
                    days = jsonObject.getString("days");
                    if (!days.equals("0")) {
                        IWPushNotifications.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                pager.setCurrentItem(3);
                            }
                        });
                    } else {
                        IWServer.getInstance().sendRequestGetPriceSubscribe(session, new IWCallback() {
                            @Override
                            public void call(Response data) {
                                try {
                                    String pricees = data.body().string();
                                    System.out.println("Price :" + pricees);
                                    JSONObject jsonObject = new JSONObject(pricees);
                                    String FAKE_SKU = jsonObject.getString("originalPriceId");

                                    Log.i("mLogs", "SKU : " + FAKE_SKU);
                                    sku = FAKE_SKU;
                                    final ArrayList<String> listPrice = new ArrayList<>();
                                    listPrice.add(sku);
                                    // TODO : Google Play  first parameter true|false


                                    ((IWApplication) getApplication()).openIabHelper.queryInventoryAsync(true, listPrice, listPrice, new IabHelper.QueryInventoryFinishedListener() {
                                        @Override
                                        public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
                                            progressDialog.dismiss();
                                            if (iabResult.isFailure()) {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(IWPushNotifications.this);
                                                builder.setTitle("Ошибка");
                                                builder.setMessage("Авторизуйтесь в Yandex.Store.");
                                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        finish();
                                                    }
                                                });
                                                builder.create().show();
                                            } else {
                                                SkuDetails skuDetails = inventory.getSkuDetails(sku);
                                                if (skuDetails != null) {
                                                    price = skuDetails.getPrice();
                                                }
                                                System.out.println("SKU : " + sku + "   Result :" + iabResult.getMessage() + " Price : " + price);

                                            }
                                            progressDialog.dismiss();
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                    IWPushNotifications.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            AlertDialog.Builder builder = new AlertDialog.Builder(IWPushNotifications.this);
                                            builder.setTitle("Ошибка");
                                            builder.setMessage("Попробуйте еще раз.");
                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    finish();
                                                }
                                            });
                                            builder.create().show();
                                        }
                                    });
                                }
                            }
                        });
                        days = "30";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        //TODO: TUTA
        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            IWFixedSpeedScroller scroller = new IWFixedSpeedScroller(pager.getContext());
            mScroller.set(pager, scroller);
        } catch (Exception e) {
        }
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {
                Log.d("mLogs", "i = " + i);
                scrolled = i == 0 ? true : false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("mLogs", "Destroued AAAA");
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {

        }
    }

    public void iwPush_ClickNext(View view) {
        if (scrolled) {
            statusNotifications = "2";
            if (view.getId() == R.id.pushnotifications_screen_fragment1_pay) {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Синхронизация...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
                final String session = preferences.getString(IWUserModel.sessionID, "none");



                // TODO: TUTA
                ((IWApplication) getApplication()).openIabHelper.launchSubscriptionPurchaseFlow(this, sku, 0, new IabHelper.OnIabPurchaseFinishedListener() {
                    @Override
                    public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
                        Log.i("mLogs", " Result  : " + iabResult.isSuccess() + " Message : " + iabResult.getMessage());
                        if (iabResult.isSuccess()) {
                            IWServer.getInstance().sendRequestSubscribe(session, statusNotifications, new IWCallback() {
                                @Override
                                public void call(Response data) {
                                    progressDialog.dismiss();
                                    IWPushNotifications.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            days = "30";
                                            if (pager.getCurrentItem() < pager.getChildCount()) {
                                                pager.setCurrentItem(pager.getCurrentItem() + 1);
                                            } else {
                                                finish();
                                            }
                                        }
                                    });

                                }
                            });
                        } else {
                            finish();
                        }
                    }
                });
            } else {
                if (pager.getCurrentItem() == 0) {
                    pager.setCurrentItem(2);
                    // В TextView забиваем ранее полученную цену
                    ((IWFragment2) adapter.getRegisteredFragment(1)).refresh();
                } else {
                    if (pager.getCurrentItem() < pager.getChildCount()) {
                        pager.setCurrentItem(pager.getCurrentItem() + 1);
                    } else {
                        finish();
                    }
                }

            }
        }
    }

    public void iwPush_ClickBack(View view) {
        if (pager.getCurrentItem() == 2) {
            pager.setCurrentItem(0);
        } else {
            if (pager.getCurrentItem() > 0) {
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        }
    }

    public void iwPush_ClickClose(View view) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ((IWApplication) getApplication()).openIabHelper.handleActivityResult(requestCode, resultCode, data);
    }

    public static class IWFragment1 extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.pushnotifications_screen_fragment1, container, false);
            TextView textTitle = (TextView) view.findViewById(R.id.pushnotifications_screen_fragment1_textmesssage);
            TextView textView = (TextView) view.findViewById(R.id.pushnotifications_screen_fragment1_textmesssage);
            textView.setText(Html.fromHtml("Позволит получать мгновенные оповещения о новых желаниях\n заказчика\n и <font color=red>бесплатные</font> заказы!"));
            return view;
        }

    }

    public static class IWFragment2 extends Fragment {
        TextView textPrice;

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.pushnotifications_screen_fragment2, container, false);
            textPrice = (TextView) view.findViewById(R.id.pushnotifications_screen_fragment2_price);
            TextView textView = (TextView) view.findViewById(R.id.pushnotifications_screen_fragment2_textmesssage);
            textView.setText(Html.fromHtml("<font color=red>Бесплатные</font> заказы !"));
            return view;
        }

        public void refresh() {
            Log.i("mLogs","REFRESHINNNG AAAAAAAAAAARHHHH");
            textPrice.setText(price);
        }
    }

    public static class IWFragment3 extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.pushnotifications_screen_fragment3, container, false);
            TextView textPrice = (TextView) view.findViewById(R.id.pushnotifications_screen_fragment3_price);
            textPrice.setText(price);
            TextView textView = (TextView) view.findViewById(R.id.pushnotifications_screen_fragment3_textmesssage);
            textView.setText(Html.fromHtml("<font color=red>Бесплатные</font> заказы !"));
            return view;
        }

        @Override
        public void onHiddenChanged(boolean hidden) {
            super.onHiddenChanged(hidden);
        }
    }

    public static class IWFragment4 extends Fragment {
        TextView textDays;

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.pushnotifications_screen_fragment4, container, false);
            textDays = (TextView) view.findViewById(R.id.pushnotifications_screen_fragment4_days);
            refresh();
            return view;
        }

        public void refresh() {
            textDays.setText(days + " дней оповещения\n осталось.");
        }
    }

    public static class IWAdapter extends FragmentStatePagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public IWAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0: {
                    return new IWFragment1();
                }
                case 1: {
                    return new IWFragment2();
                }
                case 2: {
                    return new IWFragment3();
                }
                case 3: {
                    return new IWFragment4();
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
