package dev.mabee.iwant;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import dev.mabee.iwant.utils.IWFixedSpeedScroller;
import dev.mabee.iwant.utils.IWViewPager;

import java.lang.reflect.Field;

/**
 * Created by Edisoni on 14.11.2014.
 */


public class IWGuideExecutorActivity extends FragmentActivity {

    IWViewPager pager;
    IWAdapter adapter;

    BroadcastReceiver broadcastReceiver;

    boolean scrolled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guideexecutor_screen);
        pager = (IWViewPager) findViewById(R.id.guide_screen_viewpager);
        adapter = new IWAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("dev.mabee.relog"));

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            IWFixedSpeedScroller scroller = new IWFixedSpeedScroller(pager.getContext());
            mScroller.set(pager, scroller);

        } catch (Exception e) {}
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {
                scrolled = i==2 ? true : false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }
    }

    public void iwGuideExecutorActivity_clickNext(View view) {
        if (!scrolled) {
            view.setClickable(false);
            if (pager.getCurrentItem() < pager.getChildCount()) {
                pager.setCurrentItem(pager.getCurrentItem() + 1);
            }
        }
    }

    public void iwGuideExecutorActivity_clickClose(View view) {
        finish();
        Intent intent = new Intent(this, IWActionsActivity.class);
        startActivity(intent);
    }


    public static class IWFragment1 extends Fragment {


        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view  = inflater.inflate(R.layout.guideexecutor_screen_fragment1,container,false);

            return view;
        }
    }

    public static class IWFragment2 extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.guideexecutor_screen_fragment2, container,false);

            return view;
        }

    }

    public static class IWFragment3 extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.guideexecutor_screen_fragment3, container,false);

            return view;
        }

    }


    public static class IWAdapter extends FragmentStatePagerAdapter {
        public IWAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0: {
                    return new IWFragment1();
                }
                case 1: {
                    return new IWFragment2();
                }
                case 2: {
                    return new IWFragment3();
                }
            }
            return null;
        }
        @Override
        public int getCount() {
            return 3;
        }
    }
}
