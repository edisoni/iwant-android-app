package dev.mabee.iwant;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWFixedSpeedScroller;
import dev.mabee.iwant.utils.IWUserModel;
import dev.mabee.iwant.utils.IWViewPager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.onepf.oms.appstore.googleUtils.*;
import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edisoni on 14.11.2014.
 */


public class IWPayActivity extends FragmentActivity {

    IWViewPager pager;
    IWAdapter adapter;

    String session;
    String uuid;
    static boolean free;

    static String sku;
    static String city;
    static String region;
    static String firstName;
    static String lastName;
    static String secondName;
    static String phons;
    static String text;
    static String time;
    static String entity;
    static String price;

    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_screen);
        pager = (IWViewPager) findViewById(R.id.pay_screen_viewpager);
        adapter = new IWAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("dev.mabee.relog"));

        uuid = getIntent().getExtras().getString("uuid");
        free = getIntent().getExtras().getBoolean("free");
        sku = getIntent().getExtras().getString("SKU");


        Log.i("mLogs", "SKU : " + sku);


        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            IWFixedSpeedScroller scroller = new IWFixedSpeedScroller(pager.getContext());
            mScroller.set(pager, scroller);
        } catch (Exception e) {
        }

        if (free) {
            pager.setCurrentItem(1);
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Синронизация...");
            progressDialog.show();
            sync(progressDialog);
        }
    }

    public void iwPayActivity_clickNext(View view) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синронизация...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ((IWApplication) getApplication()).openIabHelper.launchPurchaseFlow(IWPayActivity.this, sku, 0, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
                if (iabResult.isSuccess()) {
                    Log.i("mLogs", "Purchase : " + iabResult.getMessage());
                    ((IWApplication) getApplication()).consumeAll();
                    if (pager.getCurrentItem() < adapter.getCount() - 1) {
                        sync(progressDialog);
                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });
        //TODO: TUTA
    }

    public void sync(final ProgressDialog progressDialog) {
        IWServer.getInstance().sendRequestPayWant(session, uuid, new IWCallback() {
            @Override
            public void call(Response data) {
                if (data.code() == 508) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            AlertDialog.Builder builder = new AlertDialog.Builder(IWPayActivity.this);
                            builder.setTitle("Ошибка");
                            builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent("dev.mabee.relog");
                                    sendBroadcast(intent);
                                }
                            });
                            builder.create().show();

                        }
                    });
                    return;
                }
                try {
                    String body = data.body().string();
                    JSONObject jObj = new JSONObject(body);
                    firstName = jObj.getString("first_name");
                    lastName = jObj.getString("last_name");
                    secondName = jObj.getString("second_name");
                    city = jObj.getString("city");
                    region = jObj.getString("state");
                    if (!jObj.isNull("entity")) {
                        entity = jObj.getString("entity");
                    } else {
                        entity = "none";
                    }
                    phons = jObj.getString("phone_number");
                    time = "C " + jObj.getString("call_time_start") + " До " + jObj.getString("call_time_end");
                    uuid = jObj.getString("uuid");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                IWPayActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (!free) {
                            pager.setCurrentItem(pager.getCurrentItem() + 1);
                        }
                        ((IWFragmentSecond) adapter.getRegisteredFragment(1)).refresh();
                    }
                });

            }
        });
    }

    public void iwPayActivity_clickCancel(View view) {
        finish();
    }


    public void iwPayActivity_clickClose(View view) {
        setResult(5);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ((IWApplication) getApplication()).openIabHelper.handleActivityResult(requestCode, resultCode, data);
    }

    public static class IWFragmentFirst extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.pay_screen_fragmentfirst, container, false);

            if (sku != null) {
                ArrayList<String> listPrice = new ArrayList<>();
                listPrice.add(sku);
                final TextView tv = (TextView) view.findViewById(R.id.pay_screen_fragmentfirst_price);

                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Синхронизация...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                ((IWApplication) getActivity().getApplication()).consumeAll();

                ((IWApplication) getActivity().getApplication()).openIabHelper.queryInventoryAsync(true, listPrice, new IabHelper.QueryInventoryFinishedListener() {
                    @Override
                    public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
                        progressDialog.dismiss();
                        SkuDetails skuDetails = inventory.getSkuDetails(sku);
                        if (skuDetails != null) {
                            tv.setText(inventory.getSkuDetails(sku).getPrice());
                        } else {
                            tv.setText("Цена не указана");
                        }
                    }
                });
            }
            //TODO: TUTA


            return view;
        }
    }

    public static class IWFragmentSecond extends Fragment {


        TextView textNumber;
        TextView textName;
        TextView textTime;
        TextView textCity;
        TextView textRegion;
        TextView textEnt;

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.pay_screen_fragmentsecond, container, false);

            textNumber = (TextView) view.findViewById(R.id.pay_screen_fragmentsecond_number);
            textName = (TextView) view.findViewById(R.id.pay_screen_fragmentsecond_name);
            textTime = (TextView) view.findViewById(R.id.pay_screen_fragmentsecond_time);
            textCity = (TextView) view.findViewById(R.id.pay_screen_fragmentsecond_city);
            textRegion = (TextView) view.findViewById(R.id.pay_screen_fragmentsecond_region);
            textEnt = (TextView) view.findViewById(R.id.pay_screen_fragmentsecond_entity);

            textNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = textNumber.getText().toString();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    String str = "tel:" + phone;
                    intent.setData(Uri.parse(str));
                    startActivity(intent);
                }
            });


            return view;
        }

        public void refresh() {
            textName.setText(lastName + " " + firstName + " " + secondName);
            textTime.setText(time);
            textNumber.setText("+7" + phons);
            textCity.setText(city);
            textRegion.setText(region);
            if (entity != null) {
                if (entity.equals("none")) {
                    textEnt.setVisibility(View.GONE);
                } else {
                    textEnt.setVisibility(View.VISIBLE);
                    textEnt.setText(entity);
                }
            }
        }
    }

    public static class IWAdapter extends FragmentStatePagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();


        public IWAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0: {
                    return new IWFragmentFirst();
                }
                case 1: {
                    return new IWFragmentSecond();
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
