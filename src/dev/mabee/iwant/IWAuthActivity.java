package dev.mabee.iwant;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import com.squareup.okhttp.Response;
import dev.mabee.iwant.utils.IWUserModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Edisoni on 31.10.2014.
 */

public class IWAuthActivity extends Activity {
    EditText textNumber;
    BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_screen);
        textNumber = (EditText) findViewById(R.id.auth_screen_number);
        textNumber.setSelection(2);
        textNumber.setOnKeyListener(new EditText.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                boolean flag = false;
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    iwAuthActivity_ClickOk(null);
                    flag = true;
                }
                return flag;
            }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.login"));


        textNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (textNumber.getText().length() < 2) {
                    textNumber.setText("+7");
                    textNumber.setSelection(2);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {}
    }

    public void iwAuthActivity_ClickOk(View view) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Синхронизация...");
        dialog.show();

        final String number = textNumber.getText().toString().replace("+7", "");
        IWServer.getInstance().sendRequestAuth(number, new IWCallback() {
                    @Override
                    public void call(Response data) {

                        Log.i("mLogs", "" + data.code());
                        dialog.dismiss();
                        if (data.code() != 200) {
                            IWAuthActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(IWAuthActivity.this);
                                    builder.setTitle("Ошибка");
                                    builder.setMessage("Такой номер не зарегистрирован");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("OK", null);
                                    builder.create().show();
                                }
                            });
                        } else {
                            try {
                                JSONObject json = new JSONObject(new String(data.body().bytes()));
                                SharedPreferences.Editor preferences = getSharedPreferences("iwant_data", MODE_PRIVATE).edit();
                                preferences.putString(IWUserModel.userPhone, textNumber.getText().toString());
                                preferences.putString(IWUserModel.userCity, json.getString(IWUserModel.userCity));
                                preferences.putString(IWUserModel.userRegion, json.getString(IWUserModel.userRegion));
                                preferences.putString(IWUserModel.userCityID, json.getString(IWUserModel.userCityID));
                                preferences.putString(IWUserModel.userStatus, json.getString(IWUserModel.userStatus));
                                if (json.has(IWUserModel.userEntityName)) {
                                    preferences.putString(IWUserModel.userEntityName, json.getString(IWUserModel.userEntityName));
                                }
                                if (json.has(IWUserModel.userFederalDistrict)) {
                                    preferences.putString(IWUserModel.userFederalDistrict, json.getString(IWUserModel.userFederalDistrict));
                                }
                                preferences.putString(IWUserModel.userFirstName, json.getString(IWUserModel.userFirstName));
                                preferences.putString(IWUserModel.userLastName, json.getString(IWUserModel.userLastName));
                                preferences.putString(IWUserModel.userSecondName, json.getString(IWUserModel.userSecondName));
                                preferences.putString(IWUserModel.userRegion, json.getString(IWUserModel.userRegion));
                                preferences.commit();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(IWAuthActivity.this, IWActivateActivity.class);
                            intent.putExtra("number", number);
                            startActivityForResult(intent,25);
                        }
                    }
                }

        );
    }


    public void iwAuthActivity_ClickCancel(View view) {
        onBackPressed();
    }
}
