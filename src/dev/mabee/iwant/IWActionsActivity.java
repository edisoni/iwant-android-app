package dev.mabee.iwant;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.*;
import android.util.Log;
import android.util.SparseArray;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.okhttp.Response;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;
import dev.mabee.iwant.utils.IWUserModel;
import dev.mabee.iwant.utils.IWWantModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ed on 26.11.2014.
 */


public class IWActionsActivity extends FragmentActivity {

    private TabHost mTabHost;
    private TabsAdapter adapter;
    private BroadcastReceiver broadcastReceiver;
    public TextView titleView;
    static final WebSocketConnection mConnection = new WebSocketConnection();

    static boolean NEED_REFRESH = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actions_screen);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("dev.mabee.relog"));


        titleView = (TextView) findViewById(R.id.actions_screen_title);

        mTabHost = (TabHost) findViewById(R.id.actions_tabhost);
        mTabHost.setup();

        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new TabsAdapter(this, mTabHost, mViewPager);

        adapter.addTab(mTabHost.newTabSpec("act").setIndicator(createTabView(this, "Деятельность")), IWFragmentActivities.class, null);
        adapter.addTab(mTabHost.newTabSpec("ord").setIndicator(createTabView(this, "Заказы")), IWFragmentOrders.class, null);
        adapter.addTab(mTabHost.newTabSpec("oth").setIndicator(createTabView(this, "Прочие")), IWFragmentOthers.class, null);

        for(int i = 0;i<3;i++) {
            ViewGroup vg = (ViewGroup) mTabHost.getTabWidget().getChildAt(i);
            TextView textView = (TextView) vg.getChildAt(0);
            textView.setTextColor(Color.WHITE);
            textView.setTextSize(16.0f);
        }
        final String TAG = "mLogs";

        try {
            mConnection.connect("ws://i-want-app.ru:10003/IwantApplication", new WebSocketHandler() {

                @Override
                public void onOpen() {
                    Log.d(TAG, "Status: Connected");
                    mConnection.sendTextMessage("Hello, world!");
                }

                @Override
                public void onTextMessage(String payload) {

                    Log.i(TAG, "Text message : " + payload);

                    try {
                    JSONObject object = new JSONObject(payload);

                        String uuid = object.getString("data");
                        Intent intent = new Intent("iw.orders");
                        intent.putExtra("uuid",uuid);
                        sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onClose(int code, String reason) {
                Log.d(TAG, "Connection lost.");
            }
        });

        } catch (WebSocketException e) {

            Log.d(TAG, e.toString());
        }

        Bundle inte = getIntent().getExtras();
        if (inte!=null) {
            if (inte.getString("FLAG").equals("YES")) {
                mTabHost.setCurrentTab(1);
                NEED_REFRESH = true;
            }
        }
    }


    private static View createTabView(final Context context, final String text) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        tv.setText(text);
        return view;
    }

    public void iwActionsActivity_ClickBack(View view) {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            mConnection.disconnect();
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {}

    }

    public static class IWFragmentActivities extends Fragment {

        IWAdapter adapter;
        LinearLayout linearLayoutnoacts;
        LinearLayout linearLayouthaveacts;

        @Override
        public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.actions_screen_fragment_act, container, false);

            linearLayoutnoacts = (LinearLayout) v.findViewById(R.id.actions_screen_fragment_act_notacts);
            linearLayouthaveacts = (LinearLayout) v.findViewById(R.id.actions_screen_fragment_act_header);
            adapter = new IWAdapter(getActivity(), this);
            ListView listView = (ListView) v.findViewById(R.id.actions_screen_fragment_act_listview);
            listView.setAdapter(adapter);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder dialogAddAct = new AlertDialog.Builder(getActivity());
                    dialogAddAct.setMessage("Введите одно слово");

                    final EditText input = new EditText(getActivity());
                    input.setLines(1);
                    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20),new InputFilter() {
                        public CharSequence filter(CharSequence src, int start,
                                                   int end, Spanned dst, int dstart, int dend) {
                            if(src.equals("")){ // for backspace
                                return src;
                            }
                            if(src.toString().matches("[а-яА-Яa-zA-Z]+")){
                                return src;
                            }
                            return "";
                        }
                    }});
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    dialogAddAct.setView(input);
                    final AlertDialog dialog = dialogAddAct.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    input.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View view, int i, KeyEvent keyEvent) {
                            boolean flag = false;
                            if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == 0) {
                                flag = true;
                                if (input.getText().length() < 2) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage("Ведите одно слово!");
                                    builder.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                                    builder.create().show();
                                } else {
                                    SharedPreferences preferences = getActivity().getSharedPreferences("iwant_data", MODE_PRIVATE);
                                    String sid = preferences.getString(IWUserModel.sessionID, "none");
                                    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                    progressDialog.setMessage("Синхронизация");
                                    progressDialog.show();

                                    try {
                                        JSONArray array = new JSONArray();
                                        array.put(input.getText().toString());

                                        JSONObject sendData = new JSONObject();
                                        sendData.put("session", sid);
                                        sendData.put("tags", array);

                                        IWServer.getInstance().sendRequestAddTag(sendData, new IWCallback() {
                                            @Override
                                            public void call(Response data) {
                                                if (data.code()==508) {
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                            builder.setTitle("Ошибка");
                                                            builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                                            builder.setCancelable(false);
                                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                                    Intent intent = new Intent("dev.mabee.relog");
                                                                    getActivity().sendBroadcast(intent);
                                                                }
                                                            });
                                                            builder.create().show();
                                                        }
                                                    });
                                                    return;
                                                }
                                                if (data.code() == 202) {
                                                    String message = "Вы ввели запрещенные слова : ";
                                                    try {
                                                        String body = data.body().string();
                                                        JSONArray jsonArray = new JSONArray(body);
                                                        for (int i = 0; i < jsonArray.length(); i++) {
                                                            String word = jsonArray.getString(i);
                                                            message += word;
                                                            if (i + 1 != jsonArray.length()) {
                                                                message += ",";
                                                            }
                                                        }

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    final String finalMessage = message;
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                                            alertDialog.setPositiveButton("OK", null);
                                                            alertDialog.setMessage(finalMessage);
                                                            alertDialog.create().show();
                                                        }
                                                    });
                                                }
                                                sync(progressDialog);
                                                dialog.dismiss();
                                            }
                                        });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            return flag;
                        }
                    });

                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Отмена", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, int which) {
                            if (input.getText().length() < 2) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("Введите одно слово!");
                                builder.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                                builder.create().show();
                            } else {
                                SharedPreferences preferences = getActivity().getSharedPreferences("iwant_data", MODE_PRIVATE);
                                String sid = preferences.getString(IWUserModel.sessionID, "none");
                                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                progressDialog.setMessage("Синхронизация");
                                progressDialog.show();

                                try {
                                    JSONArray array = new JSONArray();
                                    array.put(input.getText().toString());

                                    JSONObject sendData = new JSONObject();
                                    sendData.put("session", sid);
                                    sendData.put("tags", array);

                                    IWServer.getInstance().sendRequestAddTag(sendData, new IWCallback() {
                                        @Override
                                        public void call(Response data) {

                                            if (data.code()==508) {
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                        builder.setTitle("Ошибка");
                                                        builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                                        builder.setCancelable(false);
                                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                Intent intent = new Intent("dev.mabee.relog");
                                                                getActivity().sendBroadcast(intent);
                                                            }
                                                        });
                                                        builder.create().show();
                                                    }
                                                });
                                                return;
                                            }
                                            if (data.code() == 202) {
                                                String message = "Вы ввели запрещенные слова : ";
                                                try {
                                                    String body = data.body().string();
                                                    JSONArray jsonArray = new JSONArray(body);
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        String word = jsonArray.getString(i);
                                                        message += word;
                                                        if (i + 1 != jsonArray.length()) {
                                                            message += ",";
                                                        }
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                final String finalMessage = message;
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                                        alertDialog.setPositiveButton("OK", null);
                                                        alertDialog.setMessage(finalMessage);
                                                        alertDialog.create().show();
                                                    }
                                                });
                                            }
                                            sync(progressDialog);
                                            dialog.dismiss();
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                    dialog.show();
                }
            };
            TextView textAdd = (TextView) v.findViewById(R.id.actions_screen_fragment_act_addact);
            TextView textAddt = (TextView) v.findViewById(R.id.actions_screen_fragment_act_addactt);
            textAdd.setOnClickListener(clickListener);
            textAddt.setOnClickListener(clickListener);

            LinearLayout pushButton = (LinearLayout) v.findViewById(R.id.actions_screen_fragment_act_push);
            pushButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), IWPushNotifications.class);
                    startActivity(intent);
                }
            });

            ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Синхронизация");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            sync(progressDialog);
            return v;
        }


        public void sync(final ProgressDialog progressDialog) {
            Log.i("mLogs", "SYNC 1");
            SharedPreferences preferences = getActivity().getSharedPreferences("iwant_data", MODE_PRIVATE);
            String session = preferences.getString(IWUserModel.sessionID , "none");

            IWServer.getInstance().sendRequestTags(session, new IWCallback() {
                @Override
                public void call(Response data) {

                    if (data.code()==508) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Ошибка");
                                builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                builder.setCancelable(false);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent("dev.mabee.relog");
                                        getActivity().sendBroadcast(intent);
                                    }
                                });
                                builder.create().show();
                            }
                        });
                        return;
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });



                    try {
                        JSONArray array = new JSONArray(data.body().string());
                        final ArrayList<String> tags = new ArrayList<String>();
                        for (int i = 0; i < array.length(); i++) {
                            tags.add(array.getString(i));
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.swapData(tags);
                                if (tags.size() > 0) {
                                    linearLayouthaveacts.setVisibility(View.VISIBLE);
                                    linearLayoutnoacts.setVisibility(View.GONE);
                                } else {
                                    linearLayouthaveacts.setVisibility(View.GONE);
                                    linearLayoutnoacts.setVisibility(View.VISIBLE);
                                }

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }

        private static class IWAdapter extends BaseAdapter {
            ArrayList<String> items = new ArrayList<>();

            private Context context;
            private IWFragmentActivities fragment;

            public IWAdapter(Context context, IWFragmentActivities fragment) {
                this.context = context;
                this.fragment = fragment;
            }

            @Override
            public int getCount() {
                return items.size();
            }

            public void swapData(ArrayList<String> list) {
                this.items = list;
                notifyDataSetChanged();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rowView = inflater.inflate(R.layout.actions_screen_fragment_act_row, parent, false);


                final TextView textText = (TextView) rowView.findViewById(R.id.actions_screen_fragment_act_row_activity);
                ImageView imageView = (ImageView) rowView.findViewById(R.id.actions_screen_fragment_act_row_image);

                final String item = items.get(position);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final AlertDialog.Builder dialogAddAct = new AlertDialog.Builder(context);
                        dialogAddAct.setTitle("Введите одно слово");
                        final EditText input = new EditText(context);
                        input.setText(textText.getText().toString());
                        input.setSelection(textText.getText().toString().length());
                        input.setImeOptions(EditorInfo.IME_ACTION_DONE);
                        input.requestFocus();
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        input.setLayoutParams(lp);
                        dialogAddAct.setView(input);
                        input.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                if (input.getText().toString().contains(" ")) {
                                    input.setText(input.getText().toString().replace(" ", ""));
                                    input.setSelection(input.getText().length());
                                }
                            }
                        });
                        AlertDialog dialog = dialogAddAct.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Отмена", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "ОК", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                final ProgressDialog progressDialog = new ProgressDialog(context);
                                progressDialog.setMessage("Синхронизация");
                                progressDialog.show();
                                try {

                                    SharedPreferences preferences = context.getSharedPreferences("iwant_data", MODE_PRIVATE);
                                    final String session = preferences.getString(IWUserModel.sessionID, "none");

                                    JSONArray array = new JSONArray();
                                    array.put(item);

                                    JSONObject sendData = new JSONObject();
                                    sendData.put("session", session);
                                    sendData.put("tags", array);

                                    IWServer.getInstance().sendRequestDeleteTag(sendData, new IWCallback() {
                                        @Override
                                        public void call(Response data) {
                                            try {
                                                String newTag = input.getText().toString();
                                                if (!newTag.isEmpty()) {
                                                    JSONArray array = new JSONArray();
                                                    array.put(newTag);

                                                    JSONObject sendData = new JSONObject();
                                                    sendData.put("session", session);
                                                    sendData.put("tags", array);

                                                    IWServer.getInstance().sendRequestAddTag(sendData, new IWCallback() {
                                                        @Override
                                                        public void call(Response data) {

                                                            if (data.code() == 202) {
                                                                String message = "Вы ввели запрещенные слова : ";
                                                                try {
                                                                    String body = data.body().string();
                                                                    JSONArray jsonArray = new JSONArray(body);
                                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                                        String word = jsonArray.getString(i);
                                                                        message += word;
                                                                        if (i + 1 != jsonArray.length()) {
                                                                            message += ",";
                                                                        }
                                                                    }

                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                                final String finalMessage = message;
                                                                fragment.getActivity().runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(fragment.getActivity());
                                                                        alertDialog.setPositiveButton("OK", null);
                                                                        alertDialog.setMessage(finalMessage);
                                                                        alertDialog.create().show();
                                                                    }
                                                                });
                                                            } else {
                                                                fragment.getActivity().runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        fragment.sync(progressDialog);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    fragment.getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            fragment.sync(progressDialog);
                                                        }
                                                    });
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });


                        dialog.show();

                    }
                });
                textText.setText(item);
                return rowView;
            }
        }
    }

    public static class IWFragmentOrders extends Fragment {

        ArrayList<IWAdapter.Item> items = new ArrayList<>();
        IWAdapter adapter;
        PullToRefreshListView listView;
        TextView textFind;
        BroadcastReceiver receiver;
        @Override
        public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.actions_screen_fragment_orders, container, false);
            adapter = new IWAdapter(getActivity());


            listView = (PullToRefreshListView) view.findViewById(R.id.actions_screen_fragment_orders_listview);
            textFind = (TextView) view.findViewById(R.id.actions_screen_fragment_orders_textfind);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    SharedPreferences preferences = getActivity().getSharedPreferences("story_orders", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    IWAdapter.Item item = (IWAdapter.Item) adapter.getItem(position);

                    if (item.active.equals("1")) {

                        try {
                            String sh = preferences.getString("orders", "none");
                            JSONArray orders = null;
                            if (sh.equals("none")) {
                                orders = new JSONArray();
                            } else {
                                orders = new JSONArray(sh);
                            }
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("uuid", item.uuid);
                            jsonObject.put("time", orders.length());
                            orders.put(jsonObject);
                            editor.putString("orders", orders.toString());
                            editor.apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    Intent intent = new Intent(getActivity(), IWAcceptWant.class);
                    intent.putExtra("desc", item.text);
                    intent.putExtra("active", item.active);
                    intent.putExtra("uuid", item.uuid);
                    intent.putExtra("city", item.city);
                    intent.putExtra("region", item.region);
                    intent.putExtra("time", "С 19:00 до 18:00");
                    intent.putExtra("wishStatus", "2");

                    startActivityForResult(intent, 0);
                }
            });

            listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                @Override
                public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                    sync(null, listView);
                }
            });


            LinearLayout buttonViewed = (LinearLayout) view.findViewById(R.id.actions_screen_fragment_orders_viewed);
            LinearLayout buttonGetMe = (LinearLayout) view.findViewById(R.id.actions_screen_fragment_orders_getme);

            buttonViewed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), IWViewedOrders.class);
                    startActivityForResult(intent, 0);
                }
            });

            buttonGetMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), IWBoughtOrders.class);
                    startActivityForResult(intent, 0);
                }
            });

            if (NEED_REFRESH) {
                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Синхронизация");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                sync(progressDialog,null);
            }

            return view;
        }

        @Override
        public void onStart() {
            super.onStart();
            IntentFilter filter = new IntentFilter("iw.orders");
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Log.i("mLogs"," ORDERS ");
                    Bundle bundle = intent.getExtras();
                    String uuid = bundle.getString("uuid");
                    ArrayList<IWAdapter.Item> its = adapter.getItems();
                    for(IWAdapter.Item item: its) {
                        if (item.uuid.equals(uuid)) {
                            item.active = "0";
                            break;
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            };
            getActivity().registerReceiver(receiver, filter);
        }

        @Override
        public void onPause() {
            try {
                getActivity().unregisterReceiver(receiver);
            } catch (Exception ex) {}
            super.onPause();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Синхронизация");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            sync(progressDialog, null);
        }

        public static class IWAdapter extends BaseAdapter {
            ArrayList<Item> items = new ArrayList<Item>();

            public static class Item implements Serializable {

                public String text;
                public String uuid;
                public String city;
                public String region;
                public String active;
                public String tags;

                public Item(String text, String city, String region, String active, String uuid, String tags) {
                    this.text = text;
                    this.city = city;
                    this.region = region;
                    this.active = active;
                    this.uuid = uuid;
                    this.tags = tags;
                }
            }

            private Context context;

            public IWAdapter(Context context) {
                this.context = context;
            }

            @Override
            public int getCount() {
                return items.size();
            }

            public ArrayList<Item> getItems() {
                return items;
            }

            public void swapData(ArrayList<Item> list) {
                this.items.clear();
                SharedPreferences preferences = context.getSharedPreferences("story_orders", MODE_PRIVATE);
                ArrayList<String> lineOrders = new ArrayList<>();
                JSONArray jsonArray = null;
                try {

                    String js = preferences.getString("orders","");

                    if (!js.isEmpty()) {

                        jsonArray = new JSONArray(js);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            lineOrders.add(jsonArray.getJSONObject(i).getString("uuid"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for (Item item : list) {
                    if (!lineOrders.contains(item.uuid)) {
                        items.add(item);
                    }
                }

                notifyDataSetChanged();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position - 1);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.actions_screen_fragment_orders_row, parent, false);

                TextView textText = (TextView) rowView.findViewById(R.id.actions_screen_fragment_orders_row_text);
                TextView textCity = (TextView) rowView.findViewById(R.id.actions_screen_fragment_orders_row_city);
                TextView textRegion = (TextView) rowView.findViewById(R.id.actions_screen_fragment_orders_row_region);
                ImageView imageView = (ImageView) rowView.findViewById(R.id.actions_screen_fragment_orders_row_image);

                Item item = items.get(position);
                textCity.setText(item.city);
                textRegion.setText(item.region);


                if (item.active.equals("0")) {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.notactive);
                    rowView.setBackgroundColor(context.getResources().getColor(R.color.grey));
                    textText.setText(item.text);
                } else {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.activered);
                    rowView.setBackgroundColor(context.getResources().getColor(R.color.white));
                    textText.setText(item.tags);
                }
                return rowView;
            }
        }

        // Empty update
        public void sync(final ProgressDialog progressDialog, final PullToRefreshListView list) {
            SharedPreferences preferences = getActivity().getSharedPreferences("iwant_data", MODE_PRIVATE);

            Log.i("mLogs", "SYNC 2" + mConnection.isConnected());

            String session  = preferences.getString(IWUserModel.sessionID, "none");
            IWServer.getInstance().sendRequestOrders(session, new IWCallback() {
                @Override
                public void call(Response data) {


                    if (data.code()==508) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Ошибка");
                                builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                builder.setCancelable(false);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent("dev.mabee.relog");
                                        getActivity().sendBroadcast(intent);
                                    }
                                });
                                builder.create().show();
                            }
                        });
                        return;
                    }


                    try {
                        String resp = data.body().string();
                        if (resp.length() > 0) {
                            items.clear();
                            JSONObject objectWants = new JSONObject(resp);
                            JSONArray array = objectWants.getJSONArray("wishes");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject objectWant = array.getJSONObject(i);


                                String tags = objectWant.getString("tags");
                                String text = objectWant.getString(IWWantModel.text);
                                String uuid = objectWant.getString(IWWantModel.uuid);
                                String city = objectWant.getString("city");
                                String region = objectWant.getString("region");
                                String active = objectWant.getString("active");

                                items.add(new IWAdapter.Item(text, city, region, active, uuid, tags));
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Activity activity = getActivity();
                    if (activity != null) {


                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (items.size() > 0) {
                                    textFind.setVisibility(View.GONE);
                                } else {
                                    textFind.setVisibility(View.VISIBLE);
                                }
                                if (progressDialog != null) {
                                    progressDialog.dismiss();
                                }
                                if (list != null) {
                                    list.onRefreshComplete();
                                }
                                adapter.swapData(items);
                            }
                        });
                    }
                }
            });
        }

    }


    public static class IWFragmentOthers extends Fragment {


        private static class IWAdapter extends BaseAdapter {
            ArrayList<Item> items = new ArrayList<Item>();

            public static class Item {

                public String text;
                public String uuid;
                public String city;
                public String region;
                public String active;
                public boolean viewed = false;

                public Item(String text, String city, String region, String active, String uuid) {
                    this.text = text;
                    this.city = city;
                    this.region = region;
                    this.active = active;
                    this.uuid = uuid;
                }
            }

            private Context context;

            public IWAdapter(Context context) {
                this.context = context;
            }
            public ArrayList<Item> getItems() {
                return items;
            }
            @Override
            public int getCount() {
                return items.size();
            }

            public void swapData(ArrayList<Item> list) {
                this.items = list;
                SharedPreferences preferences = context.getSharedPreferences("story_others", MODE_PRIVATE);
                ArrayList<String> lineOrders = new ArrayList<String>(Arrays.asList(preferences.getString("orders", "none").split(" ")));

                for (Item item : items) {
                    if (lineOrders.contains(item.uuid)) {
                        item.viewed = true;
                    } else {
                        item.viewed = false;
                    }
                }

                notifyDataSetChanged();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.actions_screen_fragment_others_row, parent, false);

                TextView textText = (TextView) rowView.findViewById(R.id.actions_screen_fragment_others_row_text);
                TextView textCity = (TextView) rowView.findViewById(R.id.actions_screen_fragment_others_row_city);
                TextView textRegion = (TextView) rowView.findViewById(R.id.actions_screen_fragment_others_row_region);
                ImageView imageView = (ImageView) rowView.findViewById(R.id.actions_screen_fragment_others_row_image);
                ImageView imageViewed = (ImageView) rowView.findViewById(R.id.actions_screen_fragment_others_row_viewed);


                Item item = items.get(position);
                textCity.setText(item.city);
                textText.setText(item.text);
                textRegion.setText(item.region);


                if (item.viewed && item.active.equals("1")) {
                    imageViewed.setVisibility(View.VISIBLE);
                } else {
                    imageViewed.setVisibility(View.INVISIBLE);
                }

                if (item.active.equals("0")) {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.notactive);
                    rowView.setBackgroundColor(context.getResources().getColor(R.color.grey));
                } else {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.active);
                    rowView.setBackgroundColor(context.getResources().getColor(R.color.white));
                }

                return rowView;
            }
        }

        ArrayList<IWAdapter.Item> items = new ArrayList<>();
        IWAdapter adapter;
        ListView listView;
        TextView textFind;
        BroadcastReceiver receiver;
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.actions_screen_fragment_others, container, false);
            adapter = new IWAdapter(getActivity());
            textFind = (TextView) view.findViewById(R.id.actions_screen_fragment_others_textfind);
            listView = (ListView) view.findViewById(R.id.actions_screen_fragment_others_listview);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    IWAdapter.Item item = (IWAdapter.Item) adapter.getItem(i);

                    Intent intent = new Intent(getActivity(), IWAcceptWant.class);
                    intent.putExtra("desc", item.text);
                    intent.putExtra("active", item.active);
                    intent.putExtra("uuid", item.uuid);
                    intent.putExtra("city", item.city);
                    intent.putExtra("region", item.region);
                    intent.putExtra("time", "С 19:00 до 18:00");
                    intent.putExtra("wishStatus", "1");


                    SharedPreferences preferences = getActivity().getSharedPreferences("story_others", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    String orders = preferences.getString("orders", "none");
                    orders += " " + item.uuid;
                    editor.putString("orders", orders);
                    editor.apply();
                    item.viewed = true;

                    startActivityForResult(intent, 0);
                }
            });


            return view;
        }

        @Override
        public void onStart() {
            super.onStart();
            IntentFilter filter = new IntentFilter("iw.orders");
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Log.i("mLogs","OTHERS");
                    Bundle bundle = intent.getExtras();
                    String uuid = bundle.getString("uuid");
                    ArrayList<IWAdapter.Item> its = adapter.getItems();
                    for(IWAdapter.Item item: its) {
                        if (item.uuid.equals(uuid)) {
                            item.active = "0";
                            break;
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            };
            getActivity().registerReceiver(receiver, filter);
        }

        @Override
        public void onPause() {
            try {
               getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }

            super.onPause();
        }


        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Синхронизация");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            sync(progressDialog);

            adapter.notifyDataSetChanged();
        }

        public void sync(final ProgressDialog progressDialog) {
            Log.i("mLogs", "SYNC 3");
            SharedPreferences preferences = getActivity().getSharedPreferences("iwant_data", MODE_PRIVATE);
            String session = preferences.getString(IWUserModel.sessionID, "none");
            IWServer.getInstance().sendRequestOtherWants(session, new IWCallback() {
                @Override
                public void call(Response data) {
                    if (data.code()==508) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Ошибка");
                                builder.setMessage("Сессия истекла, Вам нужно выполнить вход");
                                builder.setCancelable(false);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent("dev.mabee.relog");
                                        getActivity().sendBroadcast(intent);
                                    }
                                });
                                builder.create().show();
                            }
                        });
                        return;
                    }
                    try {
                        String resp = data.body().string();
                        if (resp.length() > 0) {
                            items.clear();
                            JSONObject objectWants = new JSONObject(resp);
                            JSONArray array = objectWants.getJSONArray("wishes");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject objectWant = array.getJSONObject(i);

                                String text = objectWant.getString(IWWantModel.text);
                                String uuid = objectWant.getString(IWWantModel.uuid);
                                String city = objectWant.getString("city");
                                String region = objectWant.getString("region");
                                String active = objectWant.getString("active");

                                items.add(new IWAdapter.Item(text, city, region, active, uuid));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Activity activity = getActivity();
                    if (activity!=null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (items.size() > 0) {
                                    textFind.setVisibility(View.GONE);
                                } else {
                                    textFind.setVisibility(View.VISIBLE);
                                }
                                adapter.swapData(items);
                                progressDialog.dismiss();
                            }
                        });
                    }
                }
            });
        }
    }


    public static class TabsAdapter extends FragmentStatePagerAdapter implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
        private final Context mContext;
        private final TabHost mTabHost;
        public final ViewPager mViewPager;
        private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

        static final class TabInfo {
            private final String tag;
            private final Class<?> clss;
            private final Bundle args;

            TabInfo(String _tag, Class<?> _class, Bundle _args) {
                tag = _tag;
                clss = _class;
                args = _args;
            }
        }


        static class DummyTabFactory implements TabHost.TabContentFactory {
            private final Context mContext;

            public DummyTabFactory(Context context) {
                mContext = context;
            }

            public View createTabContent(String tag) {
                View v = new View(mContext);
                v.setMinimumWidth(0);
                v.setMinimumHeight(0);
                return v;
            }
        }

        public TabsAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
            super(activity.getSupportFragmentManager());
            mContext = activity;
            mTabHost = tabHost;
            mViewPager = pager;
            mTabHost.setOnTabChangedListener(this);
            mViewPager.setAdapter(this);
            mViewPager.setOnPageChangeListener(this);
        }

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
            tabSpec.setContent(new DummyTabFactory(mContext));
            String tag = tabSpec.getTag();

            TabInfo info = new TabInfo(tag, clss, args);
            mTabs.add(info);
            mTabHost.addTab(tabSpec);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public Fragment getItem(int position) {
            TabInfo info = mTabs.get(position);
            Fragment fraga = Fragment.instantiate(mContext, info.clss.getName(), info.args);
            registeredFragments.put(position, fraga);
            return fraga;

        }

        public void onTabChanged(String tabId) {
            int position = mTabHost.getCurrentTab();
            mViewPager.setCurrentItem(position);


            if (tabId.equals("act")) {
                IWFragmentActivities fa = (IWFragmentActivities) getRegisteredFragment(0);
                if (fa != null) {
                    ProgressDialog progressDialog = new ProgressDialog(mContext);
                    progressDialog.setMessage("Синхронизация");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    ((IWActionsActivity)mContext).titleView.setText("Деятельность");
                    fa.sync(progressDialog);
                }
            }
            if (tabId.equals("ord")) {
                IWFragmentOrders fo = (IWFragmentOrders) getRegisteredFragment(1);
                if (fo != null) {
                    ProgressDialog progressDialog = new ProgressDialog(mContext);
                    progressDialog.setMessage("Синхронизация");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    ((IWActionsActivity)mContext).titleView.setText("Заказы");
                    fo.sync(progressDialog, null);
                }
            }
            if (tabId.equals("oth")) {
                IWFragmentOthers fot = (IWFragmentOthers) getRegisteredFragment(2);
                if (fot != null) {
                    ProgressDialog progressDialog = new ProgressDialog(mContext);
                    progressDialog.setMessage("Синхронизация");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    ((IWActionsActivity)mContext).titleView.setText("Прочие");
                    fot.sync(progressDialog);
                }
            }
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public void onPageSelected(int position) {
            TabWidget widget = mTabHost.getTabWidget();
            int oldFocusability = widget.getDescendantFocusability();
            widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            mTabHost.setCurrentTab(position);
            widget.setDescendantFocusability(oldFocusability);
        }

        public void onPageScrollStateChanged(int state) {

        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
