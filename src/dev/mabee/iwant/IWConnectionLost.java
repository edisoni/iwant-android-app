package dev.mabee.iwant;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by Edisoni on 09.12.2014.
 */



public class IWConnectionLost extends FragmentActivity {

    BroadcastReceiver internetReciever;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connectionlost_screen);

        IntentFilter internetFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        internetReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo wifi   = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                boolean wif = wifi.isConnectedOrConnecting();
                boolean mob = mobile.isConnectedOrConnecting();
                if (wif || mob) {
                    try {
                        unregisterReceiver(this);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    IWConnectionLost.this.finish();
                }
            }
        };
        getApplicationContext().registerReceiver(internetReciever, internetFilter);
    }

    @Override
    public void onBackPressed() {}
}
