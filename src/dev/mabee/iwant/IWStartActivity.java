package dev.mabee.iwant;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.*;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import dev.mabee.iwant.utils.IWUserModel;
import org.onepf.oms.OpenIabHelper;
import org.onepf.oms.appstore.googleUtils.IabHelper;
import org.onepf.oms.appstore.googleUtils.IabResult;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.prefs.PreferenceChangeEvent;

public class IWStartActivity extends Activity {


    Context context;
    BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);
        TextView textView = (TextView) findViewById(R.id.start_screen_text_wat);
        textView.setText(Html.fromHtml("<u>Что это такое?</u>"));



        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) {
            if (bundle.getString("FLAG") != null) {

                Intent intentClose = new Intent("dev.mabee.relog");
                intentClose.putExtra("EXIT", "NULL");
                sendBroadcast(intentClose);

                Intent intent = new Intent(getApplicationContext(), IWHomeActivity.class);
                intent.putExtra("FLAG", "YES");

                startActivityForResult(intent, 25);
                finish();
                return;
            }
        }


        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(broadcastReceiver,new IntentFilter("dev.mabee.login"));
        SharedPreferences sharedPreferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        String number = sharedPreferences.getString(IWUserModel.userPhone, "none");
        String uid = sharedPreferences.getString(IWUserModel.genUID,"none");
        String sessionID = sharedPreferences.getString(IWUserModel.sessionID,"none");

        context = getApplicationContext();

        SharedPreferences preferences = getSharedPreferences("iwant_data", MODE_PRIVATE);
        String session = preferences.getString(IWUserModel.sessionID, "0");
        IWServer.getInstance().sendRequestOrders(session, null);


        Log.i("mLogs","Application started with number : " + number);
        Log.i("mLogs","Application UID : " + uid);
        Log.i("mLogs","Application sessionID : " + sessionID);



        if (!number.equals("none")) {
            Intent intent = new Intent(getApplicationContext(), IWHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("firstStart", false);
            startActivity(intent);
            finish();
        } else {
            if (!isOnline()) {
                Intent intent = new Intent(getApplicationContext() ,IWConnectionLost.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception ex) {

        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void iwStartActivity_ClickAuth(View view) {
        Intent intent = new Intent(this, IWAuthActivity.class);
        startActivityForResult(intent, 25);
    }

    public void iwStartActivity_ClickReg(View view) {
        Intent intent = new Intent(this,IWRegActivity.class);
        startActivityForResult(intent, 25);
    }

    public void iwStartActivity_ClickAbout(View view) {
        Intent intent = new Intent(this,IWAbout.class);
        startActivityForResult(intent, 25);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==25 && resultCode==403) {
            finish();
        }
    }

}
