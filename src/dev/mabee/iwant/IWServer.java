package dev.mabee.iwant;

import android.app.AlertDialog;
import android.content.*;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import com.squareup.okhttp.*;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.onepf.oms.OpenIabHelper;
import org.onepf.oms.appstore.googleUtils.IabHelper;
import org.onepf.oms.appstore.googleUtils.IabResult;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Ed on 06.11.2014.
 */
public class IWServer {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType APPX = MediaType.parse("application/x-www-form-urlencoded");

    //** WEB API **//
    final String CONSTANT_URL_REG = "https://i-want-app.ru/registration/app/reg.php";
    final String CONSTANT_URL_EDT = "https://i-want-app.ru/registration/app/updateUser.php";
    final String CONSTANT_URL_REJ = "https://i-want-app.ru/wantes/app/rejectwant.php";
    final String CONSTANT_URL_DLT = "https://i-want-app.ru/registration/app/deleteUser.php";
    final String CONSTANT_URL_ENTER = "https://i-want-app.ru/registration/app/enter.php";
    final String CONSTANT_URL_CHECKCODE = "https://i-want-app.ru/registration/app/checkcode.php";
    final String CONSTANT_URL_REGETCODE = "https://i-want-app.ru/registration/app/regetcode.php";
    final String CONSTANT_URL_GETSID = "https://i-want-app.ru/registration/app/getsid.php";
    final String CONSTANT_URL_GETPRICEID = "https://i-want-app.ru/subscribe/getprice.php";
    final String CONSTANT_URL_GETEXPDAYS = "https://i-want-app.ru/subscribe/checksubscribe.php";
    final String CONSTANT_URL_SUBSCRIBE = "https://i-want-app.ru/subscribe/subscribe.php";
    final String CONSTANT_URL_SETTOKEN = "https://i-want-app.ru/registration/app/settoken.php";
    final String CONSTANT_URL_GETCITY = "https://i-want-app.ru/registration/app/getcities.php";
    final String CONSTANT_URL_ADDWANTS = "https://i-want-app.ru/wantes/app/addwants.php";
    final String CONSTANT_URL_DELETEWANTS = "https://i-want-app.ru/wantes/app/delwants.php";
    final String CONSTANT_URL_GETWANTS = "https://i-want-app.ru/wantes/app/getwants.php";
    final String CONSTANT_URL_ADDTAGS = "https://i-want-app.ru/tags/app/addtags.php";
    final String CONSTANT_URL_DELETETAGS = "https://i-want-app.ru/tags/app/deltags.php";
    final String CONSTANT_URL_GETTAGS = "https://i-want-app.ru/tags/app/gettags.php";
    final String CONSTANT_URL_GETOTHERTAGS = "https://i-want-app.ru/tags/app/getnotpopular.php";
    final String CONSTANT_URL_PAYWANT = "https://i-want-app.ru/wantes/app/paywant.php";
    final String CONSTANT_URL_GETUSERLIST = "https://i-want-app.ru/wantes/app/getuserslist.php";
    final String CONSTANT_URL_ADDMARK = "https://i-want-app.ru/wantes/app/addmark.php";
    final String CONSTANT_URL_FROMKNOW = "https://i-want-app.ru/registration/app/getadvSource.php";
    final String CONSTANT_URL_WANTSCOUNT = "https://i-want-app.ru/wantes/app/getwantscount.php";
    final String CONSTANT_URL_GETSTATUSWANT = "http://i-want-app.ru/wantes/app/getwantstatus.php";
    final String CONSTANT_URL_NOTIFY = "http://i-want-app.ru/wantes/app/successpush.php";

    final String CONSTANT_MYWANTS = "1";
    final String CONSTANT_ALIENWANTS = "2";
    final String CONSTANT_PAIDWANTS = "3";
    final String CONSTANT_OTHER = "4";


    private static IWServer iwServer = null;

    //**  HTTP  **//
    private OkHttpClient client;
    private Context context;

    public static IWServer getInstance() {
        if (iwServer != null) {
            return iwServer;
        } else {
            iwServer = new IWServer();
            return iwServer;
        }
    }


    private IWServer() {
        client = getUnsafeOkHttpClient();
    }

    public void setContext(Context context) {

    }

    public void sendRequestWants(String session, final IWCallback iwCallback) {

        String getQuery = CONSTANT_URL_GETWANTS + "?session=" + session +
                "&wishes_status=" + CONSTANT_MYWANTS +
                "&selection_date=" + 0;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback != null) {
                    iwCallback.call(response);
                }
            }
        });


    }

    public void sendRequestNotify(String token, final IWCallback callback) {
        String getQuery = CONSTANT_URL_NOTIFY + "?token=" + token;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (callback != null) {
                    callback.call(response);
                }
            }
        });

    }

    public void sendRequestValidate(String text, final IWCallback iwCallback) {

        String getQuery = null;
        try {
            getQuery = "http://speller.yandex.net/services/spellservice.json/checkText?text=" + URLEncoder.encode(text, HTTP.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

                Log.i("mLogs","Failed request");
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.i("mLogs","Success request");
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestGetStatusWant(String session, String uuid,final IWCallback iwCallback) {
        String getQuery = null;
        getQuery = CONSTANT_URL_GETSTATUSWANT+"?session="+session+"&uuid="+uuid;
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

                Log.i("mLogs","Failed request");
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.i("mLogs","Success request");
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestGetPrice(String session, final String wishStatus, final IWCallback iwCallback) {
        String getQuery = CONSTANT_URL_GETPRICEID + "?session=" + session + "&status=1" + "&wish_status=" + wishStatus;
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestSubscribe(String session, String status, final IWCallback iwCallback) {
        String postData = "session=" + session + "&status="+status;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("session",session);
            jsonObject.put("status",status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(APPX, jsonObject.toString().getBytes());

        final Request request = new Request.Builder().url(CONSTANT_URL_SUBSCRIBE).post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }
    public void sendRequestGetPriceSubscribe(String session, final IWCallback iwCallback) {

        String getQuery = CONSTANT_URL_GETPRICEID + "?session=" + session + "&status=2" + "&wish_status=" + 2;
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }


    public void sendRequestRCode(String number, final IWCallback callback) {
        String getQuery = CONSTANT_URL_REGETCODE + "?phone=" + number;
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
               if (callback!=null) {
                    callback.call(response);
                }
            }
        });
    }

    public void sendRequestWantCount(final IWCallback callback) {
        String getQuery = CONSTANT_URL_WANTSCOUNT;
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
               if (callback!=null) {
                   callback.call(response);
               }
            }
        });
    }

    public void sendRequestPayedWants(String session, final IWCallback iwCallback) {


        String getQuery = CONSTANT_URL_GETWANTS + "?session=" + session +
                "&wishes_status=" + CONSTANT_PAIDWANTS +
                "&selection_date=" + 0;
        final Request request = new Request.Builder().url(getQuery).get().build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestDltUser(String session, final IWCallback iwCallback) {


        String getQuery = CONSTANT_URL_DLT + "?session=" + session;

        final Request request = new Request.Builder().url(getQuery).get().build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestRejectWant(String session, final String uuid, final IWCallback iwCallback) {

        String getQuery = CONSTANT_URL_REJ + "?session=" + session + "&uuid=" + uuid;

        final Request request = new Request.Builder().url(getQuery).get().build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestFromKnow(final IWCallback callback) {
        String getQuery = CONSTANT_URL_FROMKNOW;
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (callback!=null) {
                    callback.call(response);
                }
            }
        });
    }

    public void sendRequestAddTag(JSONObject object, final IWCallback iwCallback) {
        RequestBody body = RequestBody.create(JSON, object.toString().getBytes());
        final Request request = new Request.Builder().url(CONSTANT_URL_ADDTAGS).post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestTags(String session, final IWCallback iwCallback) {
        String getQuery = CONSTANT_URL_GETTAGS + "?session=" + session;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestOtherTags(final IWCallback iwCallback) {
        String getQuery = CONSTANT_URL_GETOTHERTAGS;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestTowns(String text, String page, final IWCallback iwCallback) {
        String getQuery = CONSTANT_URL_GETCITY + "?mask=" + text +
                "&page=" + page;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestUsers(String session, final String uidWant, final IWCallback iwCallback) {

        String getQuery = CONSTANT_URL_GETUSERLIST + "?session=" + session +
                "&uuid=" + uidWant;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestDeleteWant(JSONObject object, final IWCallback iwCallback) {
        RequestBody body = RequestBody.create(JSON, object.toString().getBytes());
        final Request request = new Request.Builder().url(CONSTANT_URL_DELETEWANTS).post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestToken(String session, final String token, final IWCallback iwCallback) {


        String getQuery = CONSTANT_URL_SETTOKEN + "?session=" + session +
                "&token=" + token;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                if (iwCallback != null) {
                    iwCallback.call(null);
                }
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestDeleteTag(JSONObject tag, final IWCallback iwCallback) {
        RequestBody body = RequestBody.create(JSON, tag.toString().getBytes());
        final Request request = new Request.Builder().url(CONSTANT_URL_DELETETAGS).post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestAddWant(JSONObject object, final IWCallback iwCallback) {


        RequestBody body = RequestBody.create(JSON, object.toString().getBytes());
        final Request request = new Request.Builder().url(CONSTANT_URL_ADDWANTS).post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestAuth(String number, final IWCallback iwCallback) {
        number = number.replace("+7", "");
        String postData = "phone=" + number;

        RequestBody body = RequestBody.create(APPX, postData);

        final Request request = new Request.Builder().url(CONSTANT_URL_ENTER).post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }
    public void sendRequestGetExpired(String session, final IWCallback iwCallback) {
        String getQuery = CONSTANT_URL_GETEXPDAYS + "?session=" + session;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.i("mLogs","Failed request");
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }
    public void sendRequestOtherWants(String session, final IWCallback iwCallback) {

        String getQuery = CONSTANT_URL_GETWANTS + "?session=" + session +
                "&wishes_status=" + CONSTANT_OTHER +
                "&selection_date=" + 0;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestOrders(String session, final IWCallback iwCallback) {

        String getQuery = CONSTANT_URL_GETWANTS + "?session=" + session +
                "&wishes_status=" + CONSTANT_ALIENWANTS +
                "&selection_date=" + 0;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestPayWant(String session, final String uuid, final IWCallback iwCallback) {


        String getQuery = CONSTANT_URL_PAYWANT + "?session=" + session +  "&uuid=" + uuid;
        Log.i("mLogs"," GET QUERY : " + getQuery);
        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestActivate(String smsCode, String number, final IWCallback iwCallback) {
        number = number.replace("+7", "");
        String postData = "phone=" + number + "&code=" + smsCode;

        RequestBody body = RequestBody.create(APPX, postData);

        final Request request = new Request.Builder().url(CONSTANT_URL_CHECKCODE).post(body).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestSessionId(String phone, String genUid, final IWCallback iwCallback) {
        phone = phone.replace("+7", "");
        String postData = "phone=" + phone + "&gen_uid=" + genUid;

        RequestBody body = RequestBody.create(APPX, postData);

        final Request request = new Request.Builder().url(CONSTANT_URL_GETSID).post(body).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestMark(String session, final String uuid, final String mark, final String userID, final IWCallback iwCallback) {

        String getQuery = CONSTANT_URL_ADDMARK + "?session=" + session +
                "&uuid=" + uuid +
                "&user_id=" + userID +
                "&mark=" + mark;

        final Request request = new Request.Builder().url(getQuery).get().build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });

    }

    public void sendRequestEditProfile(JSONObject object, final IWCallback iwCallback) {
        RequestBody body = RequestBody.create(JSON, object.toString().getBytes());
        Request request = new Request.Builder().url(CONSTANT_URL_EDT).post(body).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }

    public void sendRequestRegistraion(JSONObject object, final IWCallback iwCallback) {
        RequestBody body = RequestBody.create(JSON, object.toString().getBytes());
        Request request = new Request.Builder().url(CONSTANT_URL_REG).post(body).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (iwCallback!=null) {
                    iwCallback.call(response);
                }
            }
        });
    }


    private OkHttpClient getUnsafeOkHttpClient() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
            };
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setSslSocketFactory(sslSocketFactory);
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
